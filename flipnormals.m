function fN = flipnormals(N, C, vP)
%FLINORMALS Flips normals N of cloud C towards view point vP, returns
%fliped normals

    points = C - repmat(vP, size(N,1),1);
    fN = N;
    if(0) % in direction of largest normal?
        [~,idx] = max(abs(N),[],2);
        idx = (1:size(N,1))' + (idx-1)*size(N,1);
        dir = N(idx).*points(idx) > 0;
    else
        dir = sum(N.*points,2) > 0;
    end
    fN(dir,:) = -N(dir,:);
end