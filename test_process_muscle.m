% Deprecated pipeline testing script
% clearvars

exist specfilepath;
if ~ans || isempty(specfilepath)
    specfilepath = '.\data\gen_data\muscle1\muscle1_spec2.yaml';
end
muscle_spec = ReadYaml(specfilepath);
% muscle_spec = ReadYaml('.\data\smpl_data\deltoid_smooth\deltoid_smooth_clav.yaml');
k = muscle_spec.K;
setname = muscle_spec.Name;
poiss = strcmp(muscle_spec.Rec, 'poiss');

X = [];
pX = []; % pred centroids

figure;
clf
axis vis3d
hold on
grid on
xlabel('X');
ylabel('Y');
zlabel('Z');
% TODO refactor to get better plots
for i=1: size(muscle_spec.Surfs, 2)
    %% Load data
    filepath = muscle_spec.Surfs{i};
    
    name = num2str(i);
    
    if strcmp(filepath(end-2:end), "stl")
        [cF, cV, N] = stlread(filepath);
        stlwrite(strcat('outputs/',setname,'/', name, '.stl'),...
            cF, cV)
    else
        C = dlmread(filepath);
        C = C(2:end,:);
        if poiss
            %% Compute normals
            ptCloud = pointCloud(C);
            N = pcnormals(ptCloud, 100);
            N = -reorientnormals(C, N);

            %% Reconstruct
            depth = 15;
            [F, V] = poissonsurfrec(C, N, depth);

            % Clip
            K = convhull(C(:, 1),C(:, 2),C(:, 3));
            cloud_hull = C(K, :);
            [cV, cF] = clipmesh(V, F, cloud_hull);

            stlwrite(strcat('outputs/',setname,'/', name,'_poisson',num2str(depth),'_clip.stl'),...
                cF, cV)
        else %CRUST
            cV = C;
            cF = MyCrustOpen(cV);
            stlwrite(strcat('outputs/',setname,'/', name,'_crust.stl'),...
                cF, cV)
        end
    end
    trisurf(cF, cV(:,1), cV(:,2), cV(:,3),...
        'facecolor','c','edgecolor','k', 'FaceAlpha', .0)
    
    %% Get centroinds
    Fvols = sqrt(sum((cross(cV(cF(:, 2), :) - cV(cF(:, 1), :), ...
                            cV(cF(:, 3), :) - cV(cF(:, 1), :))/2).^2, 2));
    Vol = sum(Fvols);
    Fwgths = Fvols ;
    Fcog = (cV(cF(:, 1), :) + cV(cF(:, 2), :) + cV(cF(:, 3), :)) / 3;
    [idx, X, ~, ~, Xw] = genkmeans(Fcog, k, 'weights', Fwgths, 'inx', 'on', 'init', 'maxw');
    
    scatter3(X(:, 1), X(:, 2), X(:, 3), 100*Xw/sum(Xw),... 
        'filled',...
        'MarkerEdgeColor','r',...
        'LineWidth' , 2);
    
    %% Pair centroid between surfaces
    if ~isempty(pX)
        shufX = pair_points(pX, X, .1, 0);
        for i=1:k
            plot3([pX(i,1), shufX(i, 1)],... 
                  [pX(i,2), shufX(i, 2)],... 
                  [pX(i,3), shufX(i, 3)], 'b',...
                  'LineWidth' , 2)
        end
        pX = shufX;
    else
        pX = X;
    end   
    
    dlmwrite(strcat('outputs/',setname,'/', name,'_cntrds.asc'), pX);
    
end


