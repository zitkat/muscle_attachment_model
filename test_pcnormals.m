clearvars
cloud_name = 'saddlehump';
load_cloud_data


ptCloud = pointCloud(C);
N = pcnormals(ptCloud, 100);
n = fitnormal(C);
viewP = mean(C) + n' ;

fN = reorientnormals(C, N);

%% plot the points cloud
figure;
clf
axis vis3d
hold on
grid on
xlabel('X');
ylabel('Y');
zlabel('Z');
% title('Points Cloud','fontsize',14)
scatter3(C(:,1),C(:,2),C(:,3),20,'ko','filled')
% scatter3(viewP(1),viewP(2),viewP(3),20,'ro','filled')
quiver3(C(:,1),C(:,2),C(:,3), fN(:,1), fN(:,2), fN(:,3))
quiver3(C(:,1),C(:,2),C(:,3), N(:,1), N(:,2), N(:,3))
axis vis3d
view(3)