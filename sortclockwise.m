function [order] = sortclockwise(points, c)
%SORTCLOCKWISE sorts provided points in clockwise order around center c
% so far is not really robust and uses some dirty tricks all hidden in cmpf
%   points to be sorted, uses first two coordinates
%   c - sorting center is optional, mean(points) is used otherwise
% Returns: order - new ordering of the points, use as points(order)

if nargin == 1
    c = mean(points, 1);
end

function [ord] = cmpf(a, b, c)
%CMPF Comparison function for sorting points clockwise
% It's pretty rad, don't look if you are afraid of dark angle rituals

    % points in different halfplanes
    if (a(1) - c(1) >= 0) && (b(1) - c(1) < 0)
        ord = -1;
        return
    elseif (a(1) - c(1) < 0) && (b(1) - c(1) >= 0)
        ord = 1;
        return
    elseif abs(a(1) - c(1)) < 10e-5 && abs(b(1) - c(1)) <10e-5 
        if (a(2) - c(2) >= 0) || (b(2) - c(2) >= 0)
            ord = a(2) > b(2);
            return
        end
        ord = b(2) > a(2);
        return
    end

    % compute the cross product of vectors (center -> a) x (center -> b)
    det = (a(1) - c(1)) * (b(2) - c(2)) - (b(1) - c(1)) * (a(2) - c(2));
    dphi = abs((a - c) * (b - c)'/(norm(a - c) * norm(b - c))-1); % cos acb angle -1
    dalf = abs((c - b) * (a - b)'/(norm(c - b) * norm(a - b))); % cos cba 
    dbet = abs((c - a) * (b - a)'/(norm(c - a) * norm(b - a))); % cos cab

    d1 = (a(1) - c(1)) * (a(1) - c(1)) + (a(2) - c(2)) * (a(2) - c(2));
    d2 = (b(1) - c(1)) * (b(1) - c(1)) + (b(2) - c(2)) * (b(2) - c(2));
    dd = d1 - d2;
    if  dphi > 10e-5 || dalf + dbet < 7/4  && det ~= 0 % magic
        ord = sign(det);
        return
    end

    % points a and b are on the same line from the center
    % check which point is closer to the center
    d1 = (a(1) - c(1)) * (a(1) - c(1)) + (a(2) - c(2)) * (a(2) - c(2));
    d2 = (b(1) - c(1)) * (b(1) - c(1)) + (b(2) - c(2)) * (b(2) - c(2));
    dd = d1 - d2;
    if dd==0 % they are the same point
        ord = 0;
        return
    end
    quadsgn = sign(prod(a - c)); % sort points according to quadrant
    if dd > 0 %a is further away
        ord = quadsgn;
    else
        ord = -quadsgn;
    end
end

cmp = @(pts,i,j) cmpf(pts(i,1:2), pts(j,1:2), c(1:2));


order = quicksort(points , cmp);

    

end

