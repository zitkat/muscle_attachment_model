function [tC] = proj2cyl(C)
%PROJ2CYL projects data on cylinder, wich main axis
% is parallel to x axis

[theta, rho , z] =  cart2pol(C(:, 3), C(:, 2), C(:,1));
tC = [theta, z, rho];

end
