clearvars
cloud_name = 'upon_priklad';
load_cloud_data


ptCloud = pointCloud(C);
N = pcnormals(ptCloud, 100);
N = -reorientnormals(C, N);

depth = 15;
[F, V] = poissonsurfrec(C, N, depth);

K = convhull(C(:, 1),C(:, 2),C(:, 3));
cloud_hull = C(K, :);
[cV, cF] = clipmesh(V, F, cloud_hull);


%% plot the points cloud
figure;
clf
axis vis3d
hold on
grid on
xlabel('X');
ylabel('Y');
zlabel('Z');
% title('Points Cloud','fontsize',14)
scatter3(C(:,1),C(:,2),C(:,3),20,'ko','filled')
quiver3(C(:,1),C(:,2),C(:,3), N(:,1), N(:,2), N(:,3))
trisurf(F,V(:,1),V(:,2),V(:,3),...
    'facecolor','c','edgecolor','b', 'FaceAlpha', .0)
trisurf(cF,cV(:,1),cV(:,2),cV(:,3),...
    'facecolor','c','edgecolor','r', 'FaceAlpha', .0)
axis vis3d
view(3)

stlwrite(strcat('outputs/',output_name,'/', output_name,'_poisson',num2str(depth),'_clip.stl'),...
        cF, cV)

