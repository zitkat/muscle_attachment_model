@echo off
set do_plot=0
set do_exit=exit;
set full_out_path=outputs

IF "%~1"=="-h" GOTO help

IF "%~1"=="-p" (set do_plot=1
               SHIFT
               set do_exit=0;)

set full_in_path=%~1

IF NOT "%~2"=="" set full_out_path=%~2

IF NOT EXIST "%full_out_path%" (mkdir "%full_out_path%")

set command="warning off; doplot=%do_plot%; specfilepath='%full_in_path%'; outputfolderpath='%full_out_path%'; run('main.m'); %do_exit%"
matlab /minimise /nodisplay /nosplash /nodesktop /wait /r %command%
GOTO end

:help
echo "  This is convenience script for running muscle processing, usage:                "
echo "                                                                                  " 
echo "  run_process_muscle [-p|-h] <.yaml specification file path> [outputfolder path]  "
echo "                                                                                  "
echo "      -p  plot resulting surfaces, centroids and their connectivity               "
echo "      -h  this help                                                               "
echo "  .yaml specification file path    to file with specification in yaml             "

:end