%% "Load" libraries
createmypath;

%% Read Specification file
exist specfilepath 'var';
if ~ans || isempty(specfilepath)
%     specfilepath = '.\data\smpl_data\hand\adductor_pollicis_caput_transversum-stl.yaml';
    specfilepath = '.\data\hand_data\all_muscle\flexor_digitorum_profundus_2.yaml';
end

exist outputfolderpath 'var';
if ~ans || isempty(outputfolderpath)
    outputfolderpath = 'outputs\';
end

muscle_spec = ReadYaml(specfilepath);

exist doplot 'var';
if ~ans || isempty(doplot)
    doplot = 1;
end

%% Set parameters
ks = muscle_spec.K;
n = size(muscle_spec.Surfs, 2);
setname = muscle_spec.Name;
muscle_name = muscle_spec.Name;

if isfield(muscle_spec, 'Rec')
    rec = muscle_spec.Rec;
else
    rec = "none";
end

if n==3
    file_tag = {'O', 'T', 'I'};
else
    file_tag = {'O', 'V', 'C', 'I'};
end
%% Run processing
for ik = ks
    if iscell(ik)
        k = ik{1};
    else
        k = ik;
    end
    [meshes, centroids] = process_muscle_fun(muscle_spec.Surfs, k, rec, doplot);

    %% Write outputs
    fulloutfolderpath = strcat(outputfolderpath, setname);
    if ~exist(fulloutfolderpath, 'dir')
        mkdir(fulloutfolderpath);
    end
    for j=1:n
        V = meshes{j, 1};
        F = meshes{j, 2};
        X = centroids{j};
        stlwrite(strcat(fulloutfolderpath,'/' , muscle_name, '_', file_tag{j}, '_mesh.stl'), F, V)
        dlmwrite(strcat(fulloutfolderpath,'/', muscle_name, '_', file_tag{j},'_', num2str(k),'_cntrds.asc'), X);
    end
end

% when running repeatedly from MATLAB
clear doplot specfilepath outputfolderpath