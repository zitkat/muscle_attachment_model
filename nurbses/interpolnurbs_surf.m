function [ctrlP, uknots, vknots] = interpolnurbs_surf(G, p, q)
%INTERPOLATENURBS_SURF Returns interpolation surface of the grid G of
%points
% p - degree of u curves
% q - degree of v curves
%   
% Based on 
% Piegl, L., & Tiller, W. (1996). The NURBS Book. Computer-Aided Design (Vol. 28). http://doi.org/10.1016/0010-4485(96)86819-9

[m, n, ~ ] = size(G);
assert((1 <= p) && (p <= m), ...
    "NURBS degree p and number of u-control of points m must satisfy 1 <= p <= m")
assert((1 <= q) && (q <= n), ...
    "NURBS degree q and number of v-control of points n must satisfy 1 <= q <= n")
% precompute bar_u parameters

du = sum(sqrt(sum((G(2:end,:, :) - G(1:end-1,:,:)).^2, 3)), 1);
baru = cumsum(sqrt(sum((G(2:end,:, :) - G(1:end-1,:,:)).^2, 3))./du, 1);
baru = [0; mean(baru, 2)];

dv = sum(sqrt(sum((G(:,2:end, :) - G(:,1:end-1,:)).^2, 3)), 2);
barv = cumsum(sqrt(sum((G(:,2:end, :) - G(:,1:end-1,:)).^2, 3))./dv, 2);
barv = [0 ,mean(barv, 1)];

uknots = zeros(1, m+p+1);
uknots(m+1:end) = 1;
for j=2:m-p
    uknots(j+p) = 1/p * sum(baru(j:j+p-1));
end

vknots = zeros(1, m+q+1);
vknots(m+1:end) = 1;
for j=2:m-q
    vknots(j+q) = 1/q * sum(barv(j:j+q-1));
end

Nu = zeros(n, n);
for i=1:n
    Nu(:, i) =  bspline_basis(i-1, p+1, uknots, baru);
end
[Lu, Uu, P] = lu(Nu);

R = zeros(m, n, 3);
for j=1:n
    for ax=1:3
        y = downsolve(Lu, P*G(j, :, ax)');
        R(:, j,ax) = upsolve(Uu, y);
    end
end

Nv = zeros(m, m);
for i=1:m
    Nv(:, i) =  bspline_basis(i-1, q+1, vknots, barv);
end

[Lv, Uv, P] = lu(Nv);
ctrlP = zeros(m,n,3);
for j=1:m
    for ax=1:3
        y = downsolve(Lv, P*R(:, j, ax));
        ctrlP(j, :,ax) = upsolve(Uv, y);
    end
end

% P = [G(:,1,:) ,P, G(:, end,:)];
% P = [[zeros(1,1,3), G(1,:, :),zeros(1,1,3)]; 
%                     P; 
%     [zeros(1,1,3), G(end, :, :),zeros(1,1,3)]];


end

