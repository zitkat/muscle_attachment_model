function [gridpoints, pin, pout, por, pbnd] = creategrid(I, varargin)
% createnurbsgrid   Creates quadmesh from cloud of points by adding new
% points and ommiting original points in densely populated areas,
% options gridstrat tight[xy] creates grid from cell grid with cell for
% each original point so only new points are added.
%
% I - cloud of points representing surface, which is not ocluded, i.e. it
% is graph of a function
% B - optional - boundary points of the cloud, not included in I
% If boundary B is provided it must be sorted
% Options:
%   'gridstrat' - strategy to use for creating cells in which gridpoint
%                 will be placed:
%       'regular' - regular smplx x smply cells
%       'tightx', 'tighty' or 'tightxy' - each point has its own column or cell
%   'smplx', 'smply' - grid density in x, y direction, only for regular gridstrat,
%                       default is 10, 10
%   'plot': 'on', 'off' - plot plnar projection of the grid with
%   colorcoding
%
% This is based on 
%   Leal, N., Leal, E., & Branch, J. W. (2010). Simple Method for Constructing NURBS Surfaces from Unorganized Points. 
%       In Shontz S. (Ed.), Proceedings of the 19th International Meshing Roundtable (pp. 161�175). 
%       Berlin, Heidelberg: Springer http://doi.org/10.1007/978-3-642-15414-0_10    
%
% Has complexity O(n^3) where n is largest dimension of grid
nargs = nargin;
args = varargin;

if nargs < 1 
  error('Need at least cloud of points'); 
end

B = [];
dargs = 1;
if nargs >= 2
    if ~ischar(args{1}) % we have I
        B = args{1}; % boundary is on input
        dargs = 2;
    end
end
   

if ~isempty(B)
    % if the boundary is provided we expect it to be sorted clockwise
%     c = mean(I, 1); % center for clockwise sorting
%     bndrIdxs = sortclockwise(B, c);
%     % [B(:,1), B(:,2)] = poly2cw(B(:,1), B(:,3)); % do not have licence for that :P
%     B = B(bndrIdxs, :);
else
    Bk = boundary(I(:,1), I(:,2), .2);
    Bk = Bk(1:end-1);
    B = I(Bk, :);
    I = I(setdiff(1:size(I, 1), Bk), :);
end
bndn = size(B, 1);
C = [B; I];

if rem(nargs - dargs, 2) % are remaining args pairs?
  error('Param value pairs expected.') 
end 

gridstrats = {'tightx', 'tighty', 'tightxy', 'regular'};
% some default values, they are bit out of place, but save some
% if's bellow
gridstrat = 'regular';
doplot = 0 ; % default is do not plot
smplx = 10; 
smply = 10;

for i=dargs:2:nargin - 1
    opname = args{i};
    opval = args{i+1};
    switch lower(opname) 
        case 'gridstrat'
           if ~ischar(opval) 
               error('Gridstrat must be text')
           elseif ~any(strcmpi(opval, gridstrats))
               error(strcat('Gridstrats are: ', strjoin(gridstrats)));
           else
               gridstrat = opval;
           end
        case 'smplx'
           if ~isnumeric(opval) || opval <= 0
               error('smplx must be positive integer')
           else
               smplx = opval;
           end
        case 'smply'
           if ~isnumeric(opval) || opval <= 0
               error('smply must be positive integer')
           else 
               smply = opval;  
           end
        case 'plot'
            if ~ischar(opval) 
               error('plot must be on|off')
            elseif ~any(strcmpi(opval, {'on', 'off'}))
                error('plot must be on|off')
            elseif strcmpi(opval, {'on'})
                doplot = 1;
            end
    end             
end


% extreme points for grid boundaries
% extreme cells
[minx, minxId] = min(C(:, 1));
[maxx, maxxId] = max(C(:, 1));

[miny, minyId] = min(C(:, 2));
[maxy, maxyId] = max(C(:, 2));

% build uniform cell grid 
gx = linspace(minx, maxx, smplx+1);
gy = linspace(miny, maxy, smply+1);

% or build tightly fitted grid
if (  strcmpi(gridstrat, 'tightx')... 
   || strcmpi(gridstrat, 'tighty') ...
   || strcmpi(gridstrat, 'tightxy'))
    [ngx, ngy] = fitnonunifgrid(C);
    if strcmpi(gridstrat, 'tightx')
        gx = ngx;
    end
    if strcmpi(gridstrat, 'tighty')
        gy = ngy;
    end
    if strcmpi(gridstrat, 'tightxy')
        gx = ngx;
        gy = ngy;
    end
    smplx = length(gx)-1;
    smply = length(gy)-1;
end

% build index of points in cells "memebership"
xId = sum( bsxfun( @ge, C(:, 1), gx(1:end-1) ), 2 ) ;
yId = sum( bsxfun( @ge, C(:, 2), gy(1:end-1) ), 2 ) ;
pointCell = smply * (xId - 1) + yId ;
nCells = smplx * smply;

%get all centroids, we need them anyway
[X,Y] = meshgrid((gx(1:end-1)+gx(2:end))/2, (gy(1:end-1)+gy(2:end))/2 ) ;
cntrds = [X(:), Y(:)];

% determine shape inner points index
innerIdx = inpolygon(cntrds(:, 1), cntrds(:, 2), B(:, 1), B(:, 2));

% preallocate 
gridpoints = zeros([smplx, smply, 3]);

% record types of points for weights
por = [];
pin = [];
pout = [];
pbnd = [];

for i=1:smplx
    for j=1:smply
        cellNum = (i-1)*smply + j; % cell idx to num
        if sum(pointCell==cellNum)==0 % cell without cloud point
           if innerIdx(cellNum) % cell centroid within cloud borders
               gridpoints(i, j, :) = insertpoint(C,i, j, cellNum, smplx, smply, gx, gy, cntrds, pointCell);
%                nbrhdavrgs = [nbrhdavrgs; avrgnbrhd];
               pin = [pin; [i, j]];
           else % cell centroid outside cloud border
               gridpoints(i, j, :) = [cntrds(cellNum, :), 0];
               pout = [pout; [i, j]];
           end 
        else % cell with cloud points  
            bndidx = pointCell(1:bndn)==cellNum;
            cpb = C(bndidx, :); %boundary points within cell
            if isempty(cpb)
                cps = C(pointCell==cellNum, :);
            else
                cps = cpb; % if we have boundary points in cell choose from them
                pbnd =  [pbnd; [i, j]];
            end
            [~, mini] = min(sum((cntrds(cellNum, :) - cps(:, 1:2)).^2, 2));
            gridpoints(i, j, :) = cps(mini, :);
            por = [por; [i, j]];
        end
    end
end

% overwrite points in extreme cells by grid extreme points, 
[ci, cj] = cellnm2idx(pointCell(minxId), smplx, smply);
gridpoints(ci, cj, :) = C(minxId, :);
[ci, cj] = cellnm2idx(pointCell(maxxId), smplx, smply);
gridpoints(ci, cj, :) = C(maxxId, :);

[ci, cj] = cellnm2idx(pointCell(maxyId), smplx, smply);
gridpoints(ci, cj, :) = C(maxyId, :);
[ci, cj] = cellnm2idx(pointCell(minyId), smplx, smply);
gridpoints(ci, cj, :) = C(minyId, :);


%% 2D plots
if doplot

    figure
    clf
    hold on

    % connect boundary
    plot(B(:, 1), B(:, 2), 'k', 'Linewidth', 2)

    % plot grid
    [GX, GY] = meshgrid(gx, gy);
    plot(GX, GY, 'k') % vertical
    plot(GX', GY', 'k') % horizontal

    % - Plot cell IDs.
    labels = arrayfun( @(k)sprintf( '%d', k ), 1:nCells, 'UniformOutput', false ) ;
    [X, Y]  = meshgrid( (gx(1:end-1)+gx(2:end))/2, (gy(1:end-1)+gy(2:end))/2 ) ;
    text( X(:), Y(:), labels, 'Color', 'b', 'FontSize', 8 ) ;

    scatter(C(:, 1), C(:, 2))
    if ~isempty(por)
        scatter(diag(gridpoints(por(:,1),por(:,2), 1)), ...
                diag(gridpoints(por(:,1),por(:,2), 2)), 'k', 'MarkerFaceColor','m')
    end
    if ~isempty(pin)
        scatter(diag(gridpoints(pin(:,1),pin(:,2), 1)), ...
                diag(gridpoints(pin(:,1),pin(:,2), 2)), 'k', 'MarkerFaceColor','g')
    end
    % if ~isempty(pout)
    %     scatter(diag(gridpoints(pout(:,1),pout(:,2), 1)), ...
    %             diag(gridpoints(pout(:,1), pout(:,2), 2)), 'k', 'MarkerFaceColor','c')
    % end
    if ~isempty(pbnd)
        scatter(diag(gridpoints(pbnd(:,1), pbnd(:,2), 1)), ...
                diag(gridpoints(pbnd(:,1), pbnd(:,2), 2)), 'k', 'MarkerFaceColor','r')
    end
%     if ~isempty(nbrhdavrgs)
%         scatter(nbrhdavrgs(:, 1), nbrhdavrgs(:, 2), 'k*')
%     end
% 
%     if ~isempty(pin) && ~isempty(nbrhdavrgs)
%         for i=1:length(pin(:,1))
%             plot([gridpoints(pin(i, 1), pin(i, 2),1), nbrhdavrgs(i,1)], ...
%                  [gridpoints(pin(i, 1), pin(i, 2),2), nbrhdavrgs(i,2)], 'k') 
%         end
%     end
    % plot boundary sorting center
    % scatter(c(:, 1), c(:, 2), 'r*') 
end
end


function pointin = insertpoint(C, i, j, cellNum, smplx, smply, gx, gy, cntrds, pointCell)
% insertpoint Inserts point into empty grid cell
%
nbrhpoints = [];
[l, ~]= size(nbrhpoints);
nbrhd_diam = 1;
while (l < 4) && (nbrhd_diam < smplx) && (nbrhd_diam < smplx)
nbrhdnum = getcellnbrhd(i, j, smplx, smply,  nbrhd_diam);
nbrhpoints = [nbrhpoints; C(ismember(pointCell, nbrhdnum), :)]; 
[l, ~] = size(nbrhpoints);
nbrhd_diam = nbrhd_diam + 1;
end

avrgnbrhd = mean(nbrhpoints, 1);
if ispointincell(avrgnbrhd, gx, gy, i, j) % 
    % linear interpolant version handles this too and seems nicer
    pointin = avrgnbrhd;
else
    if l <= 3
        % Var. 1 - article, place point above centroid in average
        % height of neighbouring points
        pointin = [cntrds(cellNum, :), avrgnbrhd(3)];
        % Var. 2 - place point on line between centroid and
        % neighbor. avrg.
        %intrscn = linecellintrscn(avrgnbrhd, cntrds(cellNum, :), gx, gy, i, j);
        %v = [intrscn - cntrds(cellNum, :), 0];
        % w = avrgnbrhd - [cntrds(cellNum, :), avrgnbrhd(3)];
        %pointin = [cntrds(cellNum, :), avrgnbrhd(3)] + norm(v)^2/(w*v')*w;
        %pointin = [cntrds(cellNum, :), 0] + norm(v)^2/(w*v')*w;
    else
        % Var. 4 place point above centroid on interpolation plane 
        % of neighboring points            
        F = scatteredInterpolant(nbrhpoints(:,1), nbrhpoints(:,2),nbrhpoints(:,3));
        pointin = [cntrds(cellNum, :), F(cntrds(cellNum, :))];
       
        % or alternatively move it towards avrgnbrhd 
        %intrscn = linecellintrscn(avrgnbrhd, cntrds(cellNum, :), gx, gy, i, j);       
        %pointin =  [intrscn, (-intrscn * n(1:2) +  avrgnbrhd*n)/n(3)];
    end 
end
 
end


function [nbrhdnum] = getcellnbrhd(i, j, m, n, diam)
%GETCELLNBRHD returns neighboring cells up to the "distance" diam
%    m is i index range, m is j index range
%   uses max metric
% TODO make this more efficient
nbrhdnum = zeros(1, 8*diam);
in = 1;

for dj = -diam : diam
    if (i + diam <= m) && (0 < j + dj) && (j + dj <= n)
        nbrhdnum(in) = (i+diam-1)*n+j+dj;
        in = in+1;
    end
    if (0 < i - diam) && (0 < j + dj) && (j + dj <= n)
        nbrhdnum(in) = (i-diam-1)*n+j+dj;
        in = in+1;
    end
end


for di = -diam+1 : diam-1
    if (0 < i + di) && (i + di <= m) && (j + diam <= n)
        nbrhdnum(in) = (i+di-1)*n+j+diam;
        in = in+1;
    end
    if (0 < i - di) && (i - di <= m) && (0 < j - diam)
        nbrhdnum(in) = (i+di-1)*n+j-diam;
        in = in+1;
    end
end
nbrhdnum = nbrhdnum(1:in-1);
end


function [gx, gy] = fitnonunifgrid(p)
%fitnonunifgrid creates grid in which each point has own column and row


% TODO ad option forming less dense grid
sx = sort(p(:,1));
gx = [sx(1);(sx(2:end) + sx(1:end-1))/2; sx(end)]';
sy = sort(p(:,2));
gy = [sy(1);(sy(2:end) + sy(1:end-1))/2; sy(end)]';
end


function [ci,cj] = cellnm2idx(cellNum, m, n)
%CELLNM2IDX returns i, j indicies of cell with cellNum
%   m is range of i index, n is range of j index
ci = int64(ceil(cellNum / n));
cj = cellNum - (ci-1)*n;
end


function [cellNum] = cellidx2num(i, j, n)
%CELLIDX2NUM returns cell number of cell with indicies i, j
%   n is j index range
cellNum = (i-1)*n + j;
end


function isin = ispointincell(p, gx, gy, i,j)
%ISPOINTINCELL returns 1 if point is in cell with indicies i, j
%   or for just 4 argins i can be cellNum

if nargin==4
    cellNum=i;
    smplx = length(gx) - 1;
    smply = length(gy) - 1;
    [i, j] = cellnm2idx(cellNum, smplx, smply);
end

isin =  (gx(i) <= p(1)) && (p(1) <= gx(i+1)) && ...
        (gy(j) <= p(2)) && (p(2) <= gy(j+1));

end


function [ints] = linecellintrscn(lstart, lend, xg, yg, i, j)
%LINECELLINTRSCN Summary of this function goes here
%   Detailed explanation goes here

if nargin==4
    cellNum=i;
    smplx = length(gx) - 1;
    smply = length(gy) - 1;
    [i, j] = cellnm2idx(cellNum, smplx, smply);
end
lstart = lstart(1:2);
lend = lend(1:2);

cstarts = [xg(i)  , yg(j)  ;
        xg(i+1), yg(j)  ;
        xg(i+1), yg(j+1);
        xg(i)  , yg(j+1);  ];
cends = [xg(i+1), yg(j);
        xg(i+1), yg(j+1);
        xg(i)  , yg(j+1);
        xg(i)  , yg(j);  ];
cell = [cstarts, cends];
res = lineSegmentIntersect(cell, [lstart, lend]);

for i=1:4
    if res.intAdjacencyMatrix(i, 1)
        break
    end  
end
ints = [res.intMatrixX(i, 1) res.intMatrixY(i, 1)];  

end


















