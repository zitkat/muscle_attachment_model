function [ctrlpoints, uknots] = interpolnurbs_curve(Q, p, varargin)
%INTERPOLATENURBS_CURVE computes interpolation NURBS curve of provided
% points Q
% Based on 
% Piegl, L., & Tiller, W. (1996). The NURBS Book. Computer-Aided Design (Vol. 28). http://doi.org/10.1016/0010-4485(96)86819-9
% 9.4.1 Least Squares Curve Approximation, p. 410
% We adopt notation introduced there.
%
% Q - list of points, sorted clockwise
% p - degree of the NURBS, 1<=p<=n where n is numbers of control points
% must hold
% Optional: 'closed' - to make algorithm close the curve
% Returns: 'ctrlpoints' -  control points
%          'uknots' - knot vector
close = 0;
if nargin == 4
    closed = varargin{1};
    if ~ischar(closed) 
       error('Use "closed" as last arg to get closed curve')
    elseif ~any(strcmpi(closed, 'closed'))
        error('Use "closed" as last arg to get closed curve')
    else 
        close = 1;
    end
end
n = size(Q, 1);
assert((1 <= p) && (p <= n), ...
    "NURBS degree p and number of control of points n must satisfy 1 <= p <= n")
% precompute bar_u parameters
d = sum(sqrt(sum((Q(2:end, :) - Q(1:end-1,:)).^2, 2)));
baru = [0; cumsum(sqrt(sum((Q(2:end, :) - Q(1:end-1,:)).^2, 2))/d)];

uknots = zeros(1, n+p+1);
uknots(n+1:end) = 1;
for j=2:n-p
    uknots(j+p) = 1/p * sum(baru(j:j+p-1));
end

N = zeros(n, n);
for i=1:n
    N(:, i) = bspline_basis(i-1, p+1, uknots, baru);
end
[L, U, P] = lu(N);
% norm(L*U - N)
% rcond(N)
ctrlpoints = zeros(n, 3);
for ax=1:3
    y = downsolve(L, P*Q(:, ax));
    ctrlpoints(:,ax) = upsolve(U, y);
end

% if close
%     ctrlpoints = [ctrlpoints; Q(end, :); ctrlpoints(1, :) ];
% else
%     ctrlpoints = [Q(1, :); ctrlpoints; Q(end, :)];
% end

end

