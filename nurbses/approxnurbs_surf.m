function [P, uknots, vknots] = approxnurbs_surf(G, r, s, p, q)
% appproxnurbs_surf     Approximates points in C using NURBS surface
% Based on 
% Piegl, L., & Tiller, W. (1996). The NURBS Book. Computer-Aided Design (Vol. 28). http://doi.org/10.1016/0010-4485(96)86819-9
% 9.4.1 Least Squares Surface Approximation, p. 419
% We adopt kinda notation introduced there.


assert((1 <= p) && (p <= r), ...
    "NURBS degree p and number of u-control of points r must satisfy 1 <= p <= r")
assert((1 <= p) && (p <= s), ...
    "NURBS degree p and number of v-control of points s must satisfy 1 <= p <= s")
[m, n, ~] = size(G);

du = sum(sqrt(sum((G(3:end,:, :) - G(2:end-1,:,:)).^2, 3)), 1);
baru = cumsum(sqrt(sum((G(3:end,:, :) - G(2:end-1,:,:)).^2, 3))./du, 1);
baru = [0; mean(baru, 2)];

dv = sum(sqrt(sum((G(:,3:end, :) - G(:,2:end-1,:)).^2, 3)), 2);
barv = cumsum(sqrt(sum((G(:,3:end, :) - G(:,2:end-1,:)).^2, 3))./dv, 2);
barv = [0 ,mean(barv, 1)];

uknots = zeros(1, r+p+2);
uknots(r+1:end) = 1;

ddu = (m+1)/(r-p+1);
assert(ddu >= 2, " Number of approximated points m must satisfy m > 2(r-p)+1 ")
for j=1:r-p
    i = floor(j*ddu);
    alph = j*ddu - i;
    uknots(p+j+1) = (1-alph)*baru(i-1) + alph*baru(i);
end

vknots = zeros(1, s+q+2);
vknots(s+1:end) = 1;
ddv = (n+1)/(s-q+1);
assert(ddv >= 2, " Number of approximated points m must satisfy n > 2(s-q)+1 ")
for j=1:s-p
    i = floor(j*ddv);
    alph = j*ddv - i;
    vknots(q+j+1) = (1-alph)*barv(i-1) + alph*barv(i);
end



Nu = zeros(n-2, r-1);
for i=1:r-1
    for j = 1:n-1
        Nu(j, i) =  bspline_basis(i, p, uknots, baru(j));
    end
end
NTNu = Nu'*Nu;
Lu = chol(NTNu);

T = zeros(r+1, n, 3);
% go over rows of points
RR = zeros(r-1, 3);
for j=1:n
    % construct RHS
    T(1, j, :) = G(1, j, :);
    T(r+1, j, :) = G(n, j, :);
    R = G(2:end, j,:) - bspline_basis(0, p,  uknots, baru).* G(2, j, :) - bspline_basis(r, p,  uknots, baru).* G(n, j, :);
    for ax =1:3
        for i=1:r-1
            RR(i, ax) = R(:, ax)' *  bspline_basis(i, p,  uknots, baru); 
        end
    end
    for ax = 1:3
        y = downsolve(Lu', RR(:, ax));
        T(2:end-1, j, ax) = upsolve(Lu, y); 
    end
end


Nv = zeros(m-2, s-1);
for i=1:s-1
    for j = 1:m-1
        Nv(j, i) =  bspline_basis(i, q, vknots, barv(j));
    end
end
NTNv = Nv'*Nv;
Lv = chol(NTNv);

P = zeros( r+1,  s+1,3);
for i=1:r+1
    % construct RHS
    P(i, 1, :) = T(i, 1,:);
    P(i, s+1,:) = T(i, n,:);
    R = squeeze(T(i, 2:end,:)) - squeeze(bspline_basis(0, q,  vknots, barv').* T(i, 2, :)) - squeeze(bspline_basis(s, q,  vknots, barv').* T(i, m, :));
    for ax =1:3
        for j=1:s-1
            RR(j, ax) = R(:, ax)' *  bspline_basis(j, q,  vknots, barv'); 
        end
    end
    for ax = 1:3
        y = downsolve(Lv', RR(:, ax));
        P(i, 2:end-1, ax) = upsolve(Lv, y); 
    end
end
end