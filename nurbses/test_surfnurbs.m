clearvars
B = [];
I = [];
cloud_name = "saddlehump"
load_cloud_data

%% Projection on plane
trep.n = fitnormal(C);
trep.v1 = getorth3dvec(trep.n);
trep.v2 = cross(trep.n, trep.v1);
trep.o = mean(C,1);

regplane = @(s, t) cat(3, trep.o(1) + s .* trep.v1(1) + t .* trep.v2(1),...
                          trep.o(2) + s .* trep.v1(2) + t .* trep.v2(2),...
                          trep.o(3) + s .* trep.v1(3) + t .* trep.v2(3));

T = [trep.v1, trep.v2, trep.n];
T = [T;  (-inv(T)*trep.o')' ];
T = [T, [0 0 0 1]'];
Tform = affine3d(T);

%% Get clockwise sorted boundary
if isempty(B)
    tC = transformPointsForward(Tform, C);
    Bk = boundary(tC(:,1), tC(:,2), .8);
    Bk = Bk(1:end-1);
    tB = tC(Bk, :);
    tI = tC(setdiff(1:size(tC, 1), Bk), :);
    B = C(Bk, :);
    I = C(setdiff(1:size(C, 1), Bk), :);
    bndn = size(B, 1);
else
    tB = transformPointsForward(Tform, B);
    tI = transformPointsForward(Tform, I);
    bord = sortclockwise(tB, mean(tI, 1));
    tB = tB(bord, :);
end
tC = [tB; tI];

%% Create grid
[tsurfgrid, pin, pout, por, pbnd] = creategrid(tI, tB,...
    'gridstrat', 'regular', ...
    'smplx', 30,...
    'smply', 30,...
    'plot', 'off');
[smplx, smply, ~] = size(tsurfgrid);

tsurfgridcutout = cutoutgrid(tsurfgrid, [pin; por]);
cutouttris = delaunay(tsurfgridcutout(:, 1), tsurfgridcutout(:, 2));




%% Create approximate NURBS surface
aprxdeg = 2;
aun = ceil((smplx - 2*aprxdeg) / 2);
avn = ceil((smply - 2*aprxdeg) / 2);
[tasurfctrl, asurfUknots, asurfVknots] = approxnurbs_surf(tsurfgrid, aun, avn, aprxdeg, aprxdeg);
[an, am, ~] = size(tasurfctrl);

tasurfc = nrbmak(permute(tasurfctrl, [3, 1, 2]), {asurfUknots, asurfVknots});


%% Create Interpolation NURBS surface
intdeg = 3;
intdeg = 2;
[tisurfctrl, isurfUknots, isurfVknots] = interpolnurbs_surf(tsurfgrid, intdeg, intdeg);
[iun, ivn, ~] = size(tisurfctrl);

tisurfc = nrbmak(permute(tisurfctrl, [3, 1, 2]), {isurfUknots, isurfVknots});


%% Plot
if 1
figure
clf
axis vis3d
hold on
grid on
xlabel('X');
ylabel('Y');
zlabel('Z');
% zlim([-10,10])
%grid plots
scatter3(tB(:,1), tB(:,2), tB(:,3) , 'b');
% plotgrid(tsurfgrid, pin, [], por, pbnd);
scatter3(tC(:,1), tC(:,2), tC(:,3), 'k')


% tsurf plot
u = linspace(0,1, 50);
v = linspace(0,1, 50);

% approximation tsurf plot

tas = nrbeval(tasurfc, {u,v});
COA = cat(3, ...
          zeros(length(u), length(v))*.4,...
          ones(length(u), length(v)),...
          ones(length(u), length(v))*.1);
surf(squeeze(tas(1,:,:)), squeeze(tas(2,:,:)), squeeze(tas(3,:,:)), COA, 'FaceAlpha',0.5);
% mesh(squeeze(tasurfctrl(:,:,1)),... 
%      squeeze(tasurfctrl(:,:,2)),... 
%      squeeze(tasurfctrl(:,:,3)),... 
%      "Facealpha", 0.0, 'EdgeColor', 'b')


figure
clf
axis vis3d
hold on
grid on
xlabel('X');
ylabel('Y');
zlabel('Z');
% zlim([-10,10])
%grid plots
scatter3(tB(:,1), tB(:,2), tB(:,3) , 'b');
% plotgrid(tsurfgrid, pin, [], por, pbnd);
scatter3(tC(:,1), tC(:,2), tC(:,3), 'k')
tis = nrbeval(tisurfc, {u,v});
COI = cat(3, ...
          ones(length(u), length(v))*.4,...
          zeros(length(u), length(v)),...
          ones(length(u), length(v))*.4);
surf(squeeze(tis(1,:,:)), squeeze(tis(2,:,:)), squeeze(tis(3,:,:)), COI, 'FaceAlpha',0.5);
% mesh(squeeze(tisurfctrl(:,:,1)),... 
%      squeeze(tisurfctrl(:,:,2)),... 
%      squeeze(tisurfctrl(:,:,3)),... 
%      "Facealpha", 0.0, 'EdgeColor', 'b')
end