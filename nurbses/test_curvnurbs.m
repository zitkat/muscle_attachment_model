clearvars
B = [];
I = [];
cloud_name = 'saddlehump';
load_cloud_data

%% Projection on plane
trep.n = fitnormal(C);
trep.v1 = getorth3dvec(trep.n);
trep.v2 = cross(trep.n, trep.v1);
trep.o = mean(C,1);

regplane = @(s, t) cat(3, trep.o(1) + s .* trep.v1(1) + t .* trep.v2(1),...
                          trep.o(2) + s .* trep.v1(2) + t .* trep.v2(2),...
                          trep.o(3) + s .* trep.v1(3) + t .* trep.v2(3));

T = [trep.v1, trep.v2, trep.n];
T = [T;  (-inv(T)*trep.o')'];
T = [T, [0 0 0 1]'];
Tform = affine3d(T);


%% Get clockwise sorted boundary
if isempty(B)
    tC = transformPointsForward(Tform, C);
    Bk = boundary(tC(:,1), tC(:,2), .8);
    Bk = Bk(1:end-1);
    tB = tC(Bk, :);
    tI = tC(setdiff(1:size(tC, 1), Bk), :);
    B = C(Bk, :);
    I = C(setdiff(1:size(C, 1), Bk), :);
    bndn = size(B, 1);
else
    tB = transformPointsForward(Tform, B);
    tI = transformPointsForward(Tform, I);
    bord = sortclockwise(tB, mean(tI, 1));
    tB = tB(bord, :);
end
tC = [tB; tI];


%% Approximate boundary
bndrdeg = 2;
bndrsmpl = ceil((bndn - 2*bndrdeg) / 2);
[tbndryctrl, bndruknot] = approxnurbs_curve(...
                [tB; tB(1,:)], bndrdeg, bndrsmpl, 'closed');
%                 [diag(tsurfgrid(pbnd(:,1),pbnd(:,2), 1)),... 
%                  diag(tsurfgrid(pbnd(:,1),pbnd(:,2), 2)),...
%                  diag(tsurfgrid(pbnd(:,1),pbnd(:,2), 3))],...
bndryctrl = transformPointsInverse(Tform, tbndryctrl);
tbndry = nrbmak(tbndryctrl', bndruknot);


%% Create Interpolating boundary
bndrintdeg = 3;
[tbndrintctrl, bndrintuknot] = interpolnurbs_curve([tB; tB(1,:)], bndrintdeg, 'closed');
bndrintctrl = transformPointsInverse(Tform, tbndrintctrl);

tbndrint = nrbmak(tbndrintctrl', bndrintuknot);

%% Transformed Plots

if 1
figure
clf
axis vis3d
hold on
grid on
xlabel('X');
ylabel('Y');
zlabel('Z');
% zlim([-10,10])
%grid plots
scatter3(tB(:,1), tB(:,2), tB(:,3) , 'b');
% plotgrid(tsurfgrid, pin, [], por, pbnd);
scatter3(tC(:,1), tC(:,2), tC(:,3), 'k')

u = linspace(0, 1, 150);
%boundary plots
tabs = nrbeval(tbndry, u)';
tibs = nrbeval(tbndrint, u)';
plot3(tabs(:,1), tabs(:, 2), tabs(:, 3), 'g', 'LineWidth',2)
scatter3(tbndryctrl(:,1), tbndryctrl(:,2), tbndryctrl(:,3),... 
    'MarkerFaceColor','g');

plot3(tibs(:,1), tibs(:, 2), tibs(:, 3), 'r', 'LineWidth',2)
scatter3(tbndrintctrl(:,1), tbndrintctrl(:,2), tbndrintctrl(:,3),... 
    'MarkerFaceColor','r');

plot3(tB(:, 1), tB(:, 2), tB(:, 3),'k.-', 'LineWidth', 2 )


end

