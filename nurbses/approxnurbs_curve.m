function [ctrlpoints, uknots] = approxnurbs_curve(Q, p, n, varargin)
%APPROXNURBS_CURVE Summary of this function goes here
%   Detailed explanation goes here

% Q - list of points, sorted clockwise

% Based on 
% Piegl, L., & Tiller, W. (1996). The NURBS Book. Computer-Aided Design (Vol. 28). http://doi.org/10.1016/0010-4485(96)86819-9
% 9.4.1 Least Squares Curve Approximation, p. 410
% We adopt notation introduced there.
close = 0;
if nargin == 4
    closed = varargin{1};
    if ~ischar(closed) 
       error('Use "closed" as last arg to get closed curve')
    elseif ~any(strcmpi(closed, 'closed'))
        error('Use "closed" as last arg to get closed curve')
    else 
        close = 1;
    end
end
assert((1 <= p) && (p <= n), ...
    "NURBS degree p and number of control of points n must satisfy 1 <= p <= n")
% precompute bar_u parameters
d = sum(sqrt(sum((Q(3:end, :) - Q(2:end-1,:)).^2, 2)));
baru = [0; cumsum(sqrt(sum((Q(3:end, :) - Q(2:end-1,:)).^2, 2))/d)];

% contruct knot vector
uknots = zeros(1, n+p+2);
uknots(n:end) = 1;
[m, ~] = size(Q);
dd = (m+1)/(n-p+1);
assert(dd > 2, " Number of approximated points m must satisfy m > 2(n-p)+1 ")
for j=1:n-p
    i = floor(j*dd);
    alph = j*dd - i;
    uknots(p+j+1) = (1-alph)*baru(i-1) + alph*baru(i);
end
% baru = baru(2:end-1);
% construct matrix
N = zeros(m-2, n-1);
for i=1:n-1
    for j = 1:m-1
        N(j, i) =  bspline_basis(i, p, uknots, baru(j));
    end
end
NTN = N'*N;

% construct RHS
R = Q(2:end,:) - bspline_basis(0, p,  uknots, baru).* Q(2, :) - bspline_basis(n, p,  uknots, baru).* Q(m, :);
RR = zeros(n-1, 3);
for ax =1:3
    for i=1:n-1
        RR(i, ax) = R(:, ax)' *  bspline_basis(i, p,  uknots, baru); 
    end
end
L = chol(NTN);

ctrlpoints = zeros(n-1, 3);
for ax=1:3
    y = downsolve(L', RR(:, ax));
    ctrlpoints(:,ax) = upsolve(L, y);
end

if close
    ctrlpoints = [ctrlpoints; Q(end, :); ctrlpoints(1, :) ];
else
    ctrlpoints = [Q(1, :); ctrlpoints; Q(end, :);];
end

end





