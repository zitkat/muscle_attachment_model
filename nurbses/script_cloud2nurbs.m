%% Original pipeline script for NURBS reconstruction
clearvars
B = [];
I = [];
cloud_name = 'cyl';
load_cloud_data

%% Projection on plane
trep.n = fitnormal(C);
trep.v1 = getorth3dvec(trep.n);
trep.v2 = cross(trep.n, trep.v1);
trep.o = mean(C,1);

regplane = @(s, t) cat(3, trep.o(1) + s .* trep.v1(1) + t .* trep.v2(1),...
                          trep.o(2) + s .* trep.v1(2) + t .* trep.v2(2),...
                          trep.o(3) + s .* trep.v1(3) + t .* trep.v2(3));

T = [trep.v2, trep.v1, trep.n];  %TODO it's all about placing the cylinder right, so how do we do it?
T = [T;  (-inv(T)*trep.o')'];
T = [T, [0 0 0 1]'];
Tform = affine3d(T);

%% Projection on cylinder/torus
% TODO detect ocluded surface
% TODO fit cylinder instead?
% TODO project points onto cylinder
pctC = transformPointsForward(Tform, C);
ctC = proj2cyl(pctC);
pctC = projcylback([ctC(:,1), ctC(:,2), .5+ctC(:,3)]);


%% Get clockwise sorted boundary
if isempty(B)
    tC = transformPointsForward(Tform, C);
    Bk = boundary(tC(:,1), tC(:,2), .8);
    Bk = Bk(1:end-1);
    tB = tC(Bk, :);
    tI = tC(setdiff(1:size(tC, 1), Bk), :);
    B = C(Bk, :);
    I = C(setdiff(1:size(C, 1), Bk), :);
    bndn = size(B, 1);
else
    tB = transformPointsForward(Tform, B);
    tI = transformPointsForward(Tform, I);
    bord = sortclockwise(tB, mean(tI, 1));
    tB = tB(bord, :);
end
tC = [tB; tI];


%% Approximate boundary
bndrdeg = 2;
bndrsmpl = ceil((bndn - 2*bndrdeg) / 2);
[tbndryctrl, bndruknot] = approxnurbs_curve(...
                [tB; tB(1,:)], bndrdeg, bndrsmpl, 'closed');
%                 [diag(tsurfgrid(pbnd(:,1),pbnd(:,2), 1)),... 
%                  diag(tsurfgrid(pbnd(:,1),pbnd(:,2), 2)),...
%                  diag(tsurfgrid(pbnd(:,1),pbnd(:,2), 3))],...
bndryctrl = transformPointsInverse(Tform, tbndryctrl);

tbndry = nrbmak(tbndryctrl', bndruknot);
bndry = nrbmak(bndryctrl', bndruknot);


%% Create Interpolating boundary
bndrintdeg = 3;
[tbndrintctrl, bndrintuknot] = interpolnurbs_curve([tB; tB(1,:)], bndrintdeg, 'closed');
bndrintctrl = transformPointsInverse(Tform, tbndrintctrl);

tbndrint = nrbmak(tbndrintctrl', bndrintuknot);
bndrint = nrbmak(bndrintctrl', bndrintuknot);


% error('Mid script reached, this error message was made on purpose')
%% Create grid
[tsurfgrid, pin, pout, por, pbnd] = creategrid(tI, tB,...
    'gridstrat', 'regular', ...
    'smplx', 30,...
    'smply', 30,...
    'plot', 'off');
[smplx, smply, ~] = size(tsurfgrid);

[tsurfgridcutout, cutouttris] = cutoutgrid(tsurfgrid, [pin; por]);


%% Transform gridpoints back to original position of the shape
surfgrid = zeros(smplx, smply, 3);
for i=1:smply
    surfgrid(:, i, :) = transformPointsInverse(Tform, squeeze((tsurfgrid(:, i, :))));
end
surfgridcutout = transformPointsInverse(Tform, tsurfgridcutout);


%% Create NURBS surface directly
surfdeg = 6;
surfuknots = [zeros(1, surfdeg), linspace(0,1, smplx - surfdeg+1), ones(1, surfdeg)];
surfvknots = [zeros(1, surfdeg), linspace(0,1, smply - surfdeg+1), ones(1, surfdeg)];

surfweights = ones([smplx, smply, 1]); 
for i=1:size(pbnd, 1)
    surfweights(pbnd(i,1),pbnd(i, 2)) = 1;
end

for i=1:size(pin, 1)
    surfweights(pin(i,1),pin(i, 2)) = 1;
end

for i=1:size(pout, 1)
    surfweights(pout(i,1), pout(i, 2)) = 0;
end

tsurfctrl = cat(3, tsurfgrid.*surfweights, surfweights);
surfctrl = cat(3, surfgrid.*surfweights, surfweights);
tsurfc = nrbmak(permute(tsurfctrl, [3, 1, 2]), {surfuknots, surfvknots});
surfc = nrbmak(permute(surfctrl, [3, 1, 2]), {surfuknots, surfvknots});


%% Create approximate NURBS surface
aprxdeg = 2;
aun = ceil((smplx - 2*aprxdeg) / 2);
avn = ceil((smply - 2*aprxdeg) / 2);
[tasurfctrl, asurfUknots, asurfVknots] = approxnurbs_surf(tsurfgrid, aun, avn, aprxdeg, aprxdeg);
[an, am, ~] = size(tasurfctrl);
asurfctrl = zeros(an, am, 3);
for i=1:am
    asurfctrl(:, i, :) = transformPointsInverse(Tform, squeeze((tasurfctrl(:, i, :))));
end
tasurfc = nrbmak(permute(tasurfctrl, [3, 1, 2]), {asurfUknots, asurfVknots});
asurfc = nrbmak(permute(asurfctrl, [3, 1, 2]), {asurfUknots, asurfVknots});


%% Create Interpolation surface
intdeg = 3;
intdeg = 3;
[tisurfctrl, isurfUknots, isurfVknots] = interpolnurbs_surf(tsurfgrid, intdeg, intdeg);
[iun, ivn, ~] = size(tisurfctrl);
asurfctrl = zeros(an, am, 3);
for i=1:ivn
    isurfctrl(:, i, :) = transformPointsInverse(Tform, squeeze((tisurfctrl(:, i, :))));
end
tisurfc = nrbmak(permute(tisurfctrl, [3, 1, 2]), {isurfUknots, isurfVknots});
isurfc = nrbmak(permute(isurfctrl, [3, 1, 2]), {isurfUknots, isurfVknots});


%% Fit NURBS surface evolutionary
[tesurfctrl, eweights, esurfUknots, esurfVknots] = evolvenurbs_surf(tsurfgrid, tsurfc);


%% Transformed Plots
title3D = strcat('NURBS Degree',{' '}, num2str(surfdeg), ", grid", {' '},num2str(smplx), "x", num2str(smply));
if 0
figure
clf
axis vis3d
hold on
grid on
xlabel('X');
ylabel('Y');
zlabel('Z');
% zlim([-10,10])
%grid plots
scatter3(tB(:,1), tB(:,2), tB(:,3) , 'b');
% plotgrid(tsurfgrid, pin, [], por, pbnd);
scatter3(tC(:,1), tC(:,2), tC(:,3), 'k')


% tsurf plot
u = linspace(0,1, 50);
v = linspace(0,1, 50);
tns = nrbeval(tsurfc, {u,v});
surf(squeeze(tns(1,:,:)),... 
     squeeze(tns(2,:,:)),... 
     squeeze(tns(3,:,:)),'FaceAlpha',0.5);

% approximation tsurf plot
tas = nrbeval(tasurfc, {u,v});
%surf(squeeze(tas(1,:,:)), squeeze(tas(2,:,:)), squeeze(tas(3,:,:)),'FaceAlpha',0.5);
% mesh(squeeze(tasurfctrl(:,:,1)),... 
%      squeeze(tasurfctrl(:,:,2)),... 
%      squeeze(tasurfctrl(:,:,3)),... 
%      "Facealpha", 0.0, 'EdgeColor', 'b')
tis = nrbeval(tisurfc, {u,v});
% surf(squeeze(tis(1,:,:)), squeeze(tis(2,:,:)), squeeze(tis(3,:,:)),'FaceAlpha',0.5);
% mesh(squeeze(tisurfctrl(:,:,1)),... 
%      squeeze(tisurfctrl(:,:,2)),... 
%      squeeze(tisurfctrl(:,:,3)),... 
%      "Facealpha", 0.0, 'EdgeColor', 'b')

%boundary plots
tabs = nrbeval(tbndry, u)';
tibs = nrbeval(tbndrint, u)';
plot3(tabs(:,1), tabs(:, 2), tabs(:, 3), 'g', 'LineWidth',2)

scatter3(tbndrintctrl(:,1), tbndrintctrl(:,2), tbndrintctrl(:,3),'*' ,... 
    'MarkerFaceColor','b');
% plot3(tB(:, 1), tB(:, 2), tB(:, 3), 'LineWidth',2, 'Color', 'k')
plot3(tibs(:,1), tibs(:, 2), tibs(:, 3), 'r', 'LineWidth',2)
title(strcat('Transformed:', title3D))

% scatter3(pctC(:,1), pctC(:,2), pctC(:,3), 'r')
% for i=1:size(tC, 1)
%     plot3([tC(i,1), pctC(i, 1)], [tC(i,2), pctC(i, 2)], [tC(i,3), pctC(i, 3)], 'b')
% end
% quiver3(0,0,0, 0,0,.1, 'LineWidth',2)
% quiver3(0,0,0, 0,.1,0, 'LineWidth',2)
% quiver3(0,0,0, .1,0,0, 'LineWidth',2)
end


%% Sample everything 
% u = linspace(0,2*pi,50);
% v = linspace(0.01,1,50);
% [u,v] = meshgrid(u,v);
% [x, y, z] = get_smpl_surf(u, v, base_surf, elipse_parm);

u = linspace(0,1, 100);
v = linspace(0,1, 100);
ns = nrbeval(surfc, {u,v});
as = nrbeval(asurfc, {u,v});
is = nrbeval(isurfc, {u,v});

u = linspace(0,1, 100);
bs = nrbeval(bndry, u)';

s = linspace(-20,20,10);
t = linspace(-20,20,10);
[s, t] = meshgrid(s, t);
rps = regplane(s, t);


%% Interpolated surface plot, with original if available
if 1
figure
hold on
axis vis3d
grid on
xlabel('X');
ylabel('Y');
zlabel('Z');

% scatter3(C(:,1), C(:,2), C(:,3) , 'b');
plotgrid(surfgrid, [], [], por, pbnd)
% surf(x, y, z,'FaceAlpha',0.5, 'EdgeColor', 'none')
surf(squeeze(is(1,:,:)), squeeze(is(2,:,:)), squeeze(is(3,:,:)),'FaceAlpha',0.0);

plot3(bs(:,1), bs(:, 2), bs(:, 3), 'g', 'LineWidth',2)
% scatter3(bndryctrl(:,1), bndryctrl(:,2), bndryctrl(:,3),'*' ,... 
%     'MarkerFaceColor','b');
ittitle3D = strcat('Interpol NURBS Degree',{' '}, num2str(intdeg), ", cntrl", {' '},num2str(iun), "x", num2str(ivn));
title(ittitle3D)
end

%% Approx surface plot
if 1
figure
hold on
axis vis3d
grid on
xlabel('X');
ylabel('Y');
zlabel('Z');

plotgrid(surfgrid, [], [], por, pbnd)

% surf(x, y, z,'FaceAlpha',0.5, 'EdgeColor', 'none')
surf(squeeze(as(1,:,:)), squeeze(as(2,:,:)), squeeze(as(3,:,:)),'FaceAlpha',0.0);
mesh(squeeze(asurfctrl(:,:,1)),... 
     squeeze(asurfctrl(:,:,2)),... 
     squeeze(asurfctrl(:,:,3)),... 
     "Facealpha", 0.0, 'EdgeColor', 'b')

plot3(bs(:,1), bs(:, 2), bs(:, 3), 'g', 'LineWidth',2)
scatter3(bndryctrl(:,1), bndryctrl(:,2), bndryctrl(:,3),'*' ,... 
    'MarkerFaceColor','b');
aptitle3D = strcat('Approx NURBS Degree',{' '}, num2str(aprxdeg), ", cntrl", {' '},num2str(aun), "x", num2str(avn));
title(aptitle3D)
end

%% Grid plot
if 1
figure
hold on
axis vis3d
grid on
xlabel('X');
ylabel('Y');
zlabel('Z');

scatter3(C(:,1), C(:,2), C(:,3), 'k')
plotgrid(surfgrid, pin, [], por, pbnd);
trimesh(cutouttris, surfgridcutout(:, 1), surfgridcutout(:, 2), surfgridcutout(:, 3), 'EdgeColor', 'b', 'FaceAlpha', .5);

end

%% Write out data for CADs and Meshlab
% stlwrite(strcat('outputs/',cloud_name,'/', output_name,'_orig.stl'), x, y, z, 'TRIANGULATION', 'f')
stlwrite(strcat('outputs/',output_name,'/', output_name,'_surf.stl'), ...
    squeeze(ns(1,:,:)), ...
    squeeze(ns(2,:,:)), ...
    squeeze(ns(3,:,:)), 'TRIANGULATION', 'f')
stlwrite(strcat('outputs/',output_name,'/', output_name,'_apsurf.stl'), ...
    squeeze(as(1,:,:)), ...
    squeeze(as(2,:,:)), ...
    squeeze(as(3,:,:)), 'TRIANGULATION', 'f')
stlwrite(strcat('outputs/',output_name,'/', output_name,'_intsurf.stl'), ...
    squeeze(is(1,:,:)), ...
    squeeze(is(2,:,:)), ...
    squeeze(is(3,:,:)), 'TRIANGULATION', 'f')
stlwrite(strcat('outputs/',output_name,'/', output_name,'_surfgrid.stl'), ...
    squeeze(surfgrid(:,:,1)), ...
    squeeze(surfgrid(:,:,2)), ...
    squeeze(surfgrid(:,:,3)), 'TRIANGULATION', 'f')
stlwrite(strcat('outputs/',output_name,'/', output_name,'_surfgridcut.stl'),...
        cutouttris, surfgridcutout)

% regression plane
stlwrite(strcat('outputs/',output_name,'/', output_name,'_regplane.stl'),...
           squeeze(rps(:,:,1)), ...
           squeeze(rps(:,:,2)), ...
           squeeze(rps(:,:,3)), 'TRIANGULATION', 'f')


% igesout({surfc, asurfc, bndry}, strcat('outputs/',output_name,'/', output_name))


