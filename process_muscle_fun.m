function [meshes, centroids] = process_muscle_fun(surfs, k, rec, doplot, dotrns)
%PROCESS_MUSCLE_FUN Pipeline function which puts alle the steps together:
% > Reads all the inputs from files surface by surface
% > Takes care of surface reconstruction
% > Saves its results
% > Computes centroids
% > Pairs them accordingly
% > Saves them
% > Optionally plots everything
%
%   setname - name of the muscle/dataset
%   surfs - filepaths to surface data - either .stl files of .csv
%   k - number of centroids to use
%   rec - mode of reconstruction - either 'poiss' or 'CRUST'
%   plot - to plot (1) or not to plot (0)
%  
% Returns: meshes - computed meshes as cell array: {V, F; ...}
%          centroids - computed centroids as cell array {X; ...}


%TODO refactor this darn thing - eliminate all the file writing to make it
%% Paramater processing
pX = []; % pred centroids
n = size(surfs, 2);
meshes = {n, 2};
centroids = {n};
indexes = {n};

for i=1:n
    %% Load data
    filepath = surfs{i};
    name = num2str(i);
    
    %% Get triangular mesh
    % read
    if strcmp(filepath(end-2:end), 'stl') % Use provided mesh
        [cF, cV, N] = stlread(filepath); % ASCII stl does not work!
        %     stlwrite(strcat('outputs\', setname,'\' , name, '.stl'), cF, cV)
        C = cV;
    else
        C = dlmread(filepath);
        C = C(2:end,:);
    end
    % try reconstrucion
    if ~strcmp(rec, "none")
        [cV, cF] = get_reconstructed_surf(C, rec);
    else
        if isempty(cF) || isempty(cV)
                error('ERROR: No reconstruction specified, but no meshes were provided!')
        end
    end
            
    meshes{i, 1} = cV;
    meshes{i, 2} = cF;
    
    %% Get centroinds
    % triangles volumes as weights
    Fvols = sqrt(sum((cross(cV(cF(:, 2), :) - cV(cF(:, 1), :), ...
                            cV(cF(:, 3), :) - cV(cF(:, 1), :))/2).^2, 2));
    Fwgths = Fvols ;
    Fcog = (cV(cF(:, 1), :) + cV(cF(:, 2), :) + cV(cF(:, 3), :)) / 3;
    [idx, X, ~, ~, Xw] = genkmeans(Fcog, k, 'weights', Fwgths, 'inx', 'on', 'init', 'maxw', 'maxiter', 50);
    
    
    %% Pair centroids between surfaces
    if ~isempty(pX)
        shufX = pair_points(pX, X, .1, 0);
        pX = shufX;
    else
        pX = X;
    end   
    centroids{i} = pX;
    indexes{i} = idx;
%   dlmwrite(strcat('outputs/',setname,'/', name,'_cntrds.asc'), pX);
    
end

%% Plots
if doplot
figure;
clf
axis vis3d
hold on
% grid on
% xlabel('X');
% ylabel('Y');
% zlabel('Z');
for j=1:n
    V = meshes{j, 1};
    F = meshes{j, 2};
    X = centroids{j};
    idx = indexes{j};
    
    % color map implicitly used in colors for surfce
    cmap = lines(k);
    colormap = lines(k);
    for i=1:k
        colors(i,:) = squeeze(ind2rgb(i,cmap))';
    end

    % colors for surface
    C = zeros(size(F,1),1);
    for i=1:k
        C(idx==i, 1) = i;
    end
    
    %plot surface
    T = patch( 'Vertices', V,...
               'Faces', F,... 
               'FaceVertexCData', C, 'FaceAlpha', .5);
    shading('faceted')

    %connect centroids
    if j+1 <= n
        sX = centroids{j+1};
        for i=1:k
            plot3([X(i,1), sX(i, 1)],... 
                  [X(i,2), sX(i, 2)],... 
                  [X(i,3), sX(i, 3)], 'b',...
                  'LineWidth' , 2)
        end
    
    end
    
    % plot centroids
    X = centroids{j};
    scatter3(X(:, 1), X(:, 2), X(:, 3),'r', ... 
        'filled');
    end
end
% set(gca,'Visible','off')
end

