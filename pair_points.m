function shufX = pair_points(oX, sX, scale, plot)
% PAIR_POINTS pairs points on two surfaces so that lines between them 
% are nicely distributed in space and do not cross each other, so far
% returns minimal pairing using munkres algorithm
%
% 
% oX - 'original' starting points, remain unchanged
% sX - 'succesive' end points
% scale - scale for plotting reference repers and planes
% plot - 1 to plot or 0 not to plot

% Returns: shufX - newly ordered second set of points sX

k = size(oX, 1);

% orep.n = fitnormal(oX);
% orep.v1 = getorth3dvec(orep.n);
% orep.v2 = cross(orep.n, orep.v1);
% orep.o = mean(oX,1);
% 
% oregplane = @(s, t) cat(3, orep.o(1) + s .* orep.v1(1) + t .* orep.v2(1),...
%                            orep.o(2) + s .* orep.v1(2) + t .* orep.v2(2),...
%                            orep.o(3) + s .* orep.v1(3) + t .* orep.v2(3));
% 
% srep.n = fitnormal(sX);
% srep.v1 = getorth3dvec(srep.n);
% srep.v2 = cross(srep.n, srep.v1);
% srep.o = mean(sX,1);
% 
% sregplane = @(s, t) cat(3, srep.o(1) + s .* srep.v1(1) + t .* srep.v2(1),...
%                            srep.o(2) + s .* srep.v1(2) + t .* srep.v2(2),...
%                            srep.o(3) + s .* srep.v1(3) + t .* srep.v2(3));

%% V1: start at the most distant/closest point and search for his closest
%   counterpart

% skdT = KDTreeSearcher(sX);
% okdT = KDTreeSearcher(oX);
% wsX = sX;
% [o2s, D] = knnsearch(skdT, oX);
% [D , Dord] = sort(D);
% Dord = Dord(end:-1:1);
% shufidX = zeros(size(sX));
% 
% for i=1:k
%    sidx = knnsearch(skdT, oX(Dord(i), :));
%    shufidX(Dord(i), :) = wsX(sidx, :);
%    wsX(sidx, :) = [];
%    skdT = KDTreeSearcher(wsX);
% end

%% V2: minimal pairing - this seems to be the thing
distM = getdistmatrix(oX, sX);
shufidX = munkres(distM);

%% V3: move surface along muscle path
%try minimal pairing on shifted points
% shift = srep.o - orep.o;
% shiftoX = oX + shift;
% distM = getdistmatrix(shiftoX, sX);
% shufidX = munkres(distM);


shufX = sX(shufidX, :);


%% Plot the points cloud
if plot
    figure;
    clf
    axis vis3d
    hold on
    grid on
    xlabel('X');
    ylabel('Y');
    zlabel('Z');

    % scale = .1;

%     u = linspace(-scale, scale, 50);
%     v = linspace(-scale,scale, 50);
%     [u, v] = meshgrid(u,v);

%     orps = oregplane(u, v);
%     srps = sregplane(u, v);

%     surf(squeeze(orps(:,:,1)), ...
%          squeeze(orps(:,:,2)), ...
%          squeeze(orps(:,:,3)),...
%          'FaceAlpha',0.3);
%     quiver3(orep.o(1),orep.o(2),orep.o(3), ...
%             1/2*scale*orep.v2(1),1/2*scale*orep.v2(2),1/2*scale*orep.v2(3), 'LineWidth',2)
%     quiver3(orep.o(1),orep.o(2),orep.o(3), ...
%             1/2*scale*orep.v1(1),1/2*scale*orep.v1(2),1/2*scale*orep.v1(3), 'LineWidth',2)
%     quiver3(orep.o(1),orep.o(2),orep.o(3), ...
%             1/2*scale*orep.n(1), 1/2*scale*orep.n(2), 1/2*scale*orep.n(3), 'LineWidth',2)

%     surf(squeeze(srps(:,:,1)), ...
%          squeeze(srps(:,:,2)), ...
%          squeeze(srps(:,:,3)),...
%          'FaceAlpha',0.3);
%     quiver3(srep.o(1),srep.o(2),srep.o(3), ...
%             1/2*scale*srep.v2(1),1/2*scale*srep.v2(2),1/2*scale*srep.v2(3), 'LineWidth',2)
%     quiver3(srep.o(1),srep.o(2),srep.o(3), ...
%             1/2*scale*srep.v1(1),1/2*scale*srep.v1(2),1/2*scale*srep.v1(3), 'LineWidth',2)
%     quiver3(srep.o(1),srep.o(2),srep.o(3), ...
%             1/2*scale*srep.n(1), 1/2*scale*srep.n(2), 1/2*scale*srep.n(3), 'LineWidth',2)

    for i=1:k
        plot3([oX(i,1), shufX(i, 1)],... 
              [oX(i,2), shufX(i, 2)],... 
              [oX(i,3), shufX(i, 3)], 'k')
    end

    scatter3(sX(:, 1), sX(:, 2), sX(:, 3), 10, 'filled',...
    'MarkerEdgeColor','b',...
    'LineWidth' , 2);
    scatter3(oX(:, 1), oX(:, 2), oX(:, 3), 10, 'filled',...
    'MarkerEdgeColor','r',...
    'LineWidth' , 2);
    % scatter3(shiftoX(:, 1), shiftoX(:, 2), shiftoX(:, 3), 10, 'filled',...
    % 'MarkerEdgeColor','g',...
    % 'LineWidth' , 2);
end    

end