function [cV, cF] = get_reconstructed_surf(cloud, rec)
%GET_RECONSTRUCTED_SURF gets cloud of points and returns surface
% reconstructed from them
% try reconstrucion
C = cloud;
    switch rec 
        case 'Poiss' % Possion reconstruction
            fprintf("WARNING: Poisson reconstruction is very slow and ureliable!")
            % Compute normals
            ptCloud = pointCloud(C);
            N = pcnormals(ptCloud, 100);
            N = -reorientnormals(C, N);

            % Reconstruct
            depth = 15;
            [F, V] = poissonsurfrec(C, N, depth);

            % Clip
            K = convhull(C(:, 1),C(:, 2),C(:, 3));
            cloud_hull = C(K, :);
            [cV, cF] = clipmesh(V, F, cloud_hull);

%             stlwrite(strcat(outputfolderpath, setname ,'/', name,'_poisson',num2str(depth),'_clip.stl'),...
%                 cF, cV) % TODO do we want to retain information about used reconstruction?
        case 'CRUST' % CRUST reconstruction
            cV = C;
            cF = MyCrustOpen(cV);
%             stlwrite(strcat(outputfolderpath, setname ,'/', name,'_crust.stl'),...
%                 cF, cV)
        otherwise
            error('ERROR: Unknwon reconstruction specified!')
    end
end

