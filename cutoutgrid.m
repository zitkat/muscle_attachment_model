function [V, tri] = cutoutgrid(grid, keep)
%CUTOUTGRID selects points in non empty list of indicies passed to it
%   creates triangulation based of rectangular grid
%   returns list of points and said triangulation
% grid - meshgrid, m x n x 3
% keep - list of grid points to keep, k x 2, values ranges 1..m and 1..n

[m, n, ~] = size(grid);
V = [];
if ~isempty(keep)
    V = [    diag(grid(keep(:,1),keep(:,2), 1)),...
             diag(grid(keep(:,1),keep(:,2), 2)),...
             diag(grid(keep(:,1),keep(:,2), 3))];
end   

keepers = zeros(m+1, n+1);
for i=1:size(keep, 1)
    keepers(keep(i,1), keep(i, 2)) = i;
end

tri = [];
for i=1:size(keep, 1)
    ki = keep(i, 1);
    kj = keep(i, 2);
    if keepers(ki+1, kj+1) > 0
        if keepers(ki, kj+1) > 0
            tri = [tri; i, keepers(ki+1, kj+1), keepers(ki, kj+1)];
        end
        if keepers(ki+1, kj) > 0
            tri = [tri; i, keepers(ki+1, kj+1), keepers(ki+1, kj)];
        end
    elseif (keepers(ki, kj+1) > 0) && (keepers(ki+1, kj) > 0)
        tri = [tri; i, keepers(ki, kj+1), keepers(ki+1, kj)];
    end   
end





end

