%% Testing of crust open surface reconstruction
clearvars
B = [];
I = [];
cloud_name = 'cyl';
load_cloud_data

%% Run  program
[tris] = MyCrustOpen(C);

%% plot the points cloud
figure;
clf
axis vis3d
hold on
grid on
xlabel('X');
ylabel('Y');
zlabel('Z');
% title('Points Cloud','fontsize',14)
scatter3(C(:,1),C(:,2),C(:,3),20,'ko','filled')
axis vis3d
view(3)


%% plot the output triangulation
figure
clf
axis vis3d
hold on
grid on
xlabel('X');
ylabel('Y');
zlabel('Z');
% title('Output Triangulation','fontsize',14)
axis equal
trisurf(tris,C(:,1),C(:,2),C(:,3),...
    'facecolor','c','edgecolor','b', 'FaceAlpha', .0)%plot della superficie
axis vis3d
view(3)

%%
stlwrite(strcat('outputs/',output_name,'/', output_name,'_crusttri.stl'),...
        tris, C)