% generates elipse slapped onto horizontal paraboloid

% elipse parametrization
a=2;
b=10;
parm_surf = {
    @(u, v)a*cos(u).*v + 10;
    @(u, v)b*sin(u).*v + 2;
    };

% paraboloid parametrization
A = [0, 0, 0];
parab_surf ={ @(u, v)A(1) + u.*u/10 + v.*v/10; %X
              @(u, v)A(2) + u; %Y
              @(u, v)A(3) + v; %Z
            };
       
% u = linspace(-4,4,50);
% v = linspace(-4,4,50);
% [u,v] = meshgrid(u, v);

u = linspace(0,2*pi,50);
v = linspace(0.01,1,50);
[u,v] = meshgrid(u,v);
[x, y, z] = get_smpl_surf(u, v, parab_surf, parm_surf);

u = linspace(-15,15,50);
v = linspace(-15,15,50);
[u,v] = meshgrid(u, v);
[px, py, pz] = get_smpl_surf(u, v, parab_surf);


% elipse boundary uniform sampling with border
nbdn = 40;
nlay = 2; % number of layers
npay = 10; % number of points per layer
ru = [linspace(0,1,nbdn)'*2*pi; repmat(linspace(0,1, npay)*2*pi,1, nlay)'];
rvi = zeros(1, npay*nlay);
for i=1:nlay
    rvi((i-1)*npay+1:i*npay) = repmat(i/(nlay+1),1,npay);
end
rv = [ones([nbdn, 1]); rvi'];

% elipse random sampling
% ru = [rand(20,1)*2*pi; rand(20,1)*2*pi];
% rv = [ones([20, 1]); rand(20, 1)];

[rx, ry, rz] = get_smpl_surf(ru, rv, parab_surf, parm_surf);
% figure options
clf
hold on
axis vis3d
grid on
xlabel('X');
ylabel('Y');
zlabel('Z');

mesh(px, py, pz, 'edgecolor', 'k')
mesh(x, y, z)
scatter3(rx, ry, rz ,... 
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','r');
csvwrite("parab_elipse_unf.csv", [rx, ry, rz])