function [nsfx,nsfy,nsfz] = get_nsf_sampl(nbnd, nlay, nplay, base_surf,elipse_parm)
%GET_NSF_SAMPL Summary of this function goes here
%   Detailed explanation goes here
% nbnd = 300; % number of points on boundary
% nlay = 45;  % number of layers
% nplay = 45; % number of points per layer
p = 5; % shift parameter
sfu = [(linspace(0,1,nbnd)*2*pi + rand(1, nbnd)/p-1/2/p)'; 
       (repmat(linspace(0, 1, nplay), 1, nlay)*2*pi + 2*pi*rand(1, nplay*nlay)/p-1/2/p)'];
sfvi = zeros(1, nplay*nlay);
for i=1:nlay
    sfvi((i-1)*nplay+1:i*nplay) = repmat(i/(nlay+1),1,nplay)+ rand(1, nplay)/p-1/2/p;
end
sfv = [ones([nbnd, 1]); sfvi'];

[sfx, sfy, sfz] = get_smpl_surf(sfu, sfv, base_surf, elipse_parm);

noisy =  [sfx, sfy, sfz] + (rand(nbnd+nlay*nplay , 3)-1/2)/5;
nsfx = noisy(:,1);
nsfy = noisy(:,2);
nsfz = noisy(:,3);
end

