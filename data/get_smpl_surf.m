function [X, Y, Z] = get_smpl_surf(u, v, proj_surf,  parm_surf)
% get_smpl_surf returns sample of points on proj_surf
% within parm_surf projected on it

if nargin==3
    id_surf = {
        @(u, v)u;
        @(u, v)v;
        };
    parm_surf = id_surf;
end

X = proj_surf{1}(parm_surf{1}(u,v), parm_surf{2}(u,v));
Y = proj_surf{2}(parm_surf{1}(u,v), parm_surf{2}(u,v));
Z = proj_surf{3}(parm_surf{1}(u,v), parm_surf{2}(u,v));
end

