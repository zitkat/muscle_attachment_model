%generate elipse slapped onto various surfaces
clearvars
%% load surfaces parametrizations
surf_parametrizations;

% Choose test surface! %
test_surf = cone_surf;
% Choose test surface! %

%% surf choice and sampling parametrers
base_surf = test_surf.base_surf;
elipse_parm = test_surf.elipse_parm;
label = test_surf.name;

nbdn = 300; % number of points on boundary
nlay = 45;  % number of layers
npay = 45; % number of points per layer


%% elipse boundary uniform sampling with boundary
fu = [linspace(0,1,nbdn)'*2*pi; repmat(linspace(0,1, npay)*2*pi,1, nlay)'];
fvi = zeros(1, npay*nlay);
for i=1:nlay
    fvi((i-1)*npay+1:i*npay) = repmat(i/(nlay+1),1,npay);
end
fv = [ones([nbdn, 1]); fvi'];


%% elipse boundary semi-uniform sampling with boundary
p = 5; % shift parameter
sfu = [(linspace(0,1,nbdn)*2*pi + rand(1, nbdn)/p-1/2/p)'; 
       (repmat(linspace(0, 1, npay), 1, nlay)*2*pi + 2*pi*rand(1, npay*nlay)/p-1/2/p)'];
sfvi = zeros(1, npay*nlay);
for i=1:nlay
    sfvi((i-1)*npay+1:i*npay) = repmat(i/(nlay+1),1,npay)+ rand(1, npay)/p-1/2/p;
end
sfv = [ones([nbdn, 1]); sfvi'];


%% elipse random sampling with boundary
ru = [rand(nbdn,1)*2*pi; rand(nbdn,1)*2*pi];
rv = [ones([nbdn, 1]); rand(nbdn,1)];

%% get sampling
[rx, ry, rz] = get_smpl_surf(ru, rv, base_surf, elipse_parm);
[fx, fy, fz] = get_smpl_surf(fu, fv, base_surf, elipse_parm);      
[sfx, sfy, sfz] = get_smpl_surf(sfu, sfv, base_surf, elipse_parm);


%% add noise
noisy =  [sfx, sfy, sfz] + (rand(nbdn+nlay*npay , 3)-1/2)/5;
nsfx = noisy(:,1);
nsfy = noisy(:,2);
nsfz = noisy(:,3);


%% write
dlmwrite(strcat(label, "_elipse_rnd.asc"),  [nbdn, nlay, npay; rx, ry, rz], '\t')
dlmwrite(strcat(label, "_elipse_unf.asc"),  [nbdn, nlay, npay; fx, fy, fz], '\t')
dlmwrite(strcat(label, "_elipse_sunf.asc"), [nbdn, nlay, npay; sfx, sfy, sfz], '\t')
dlmwrite(strcat(label, "_elipse_nsunf.asc"),[nbdn, nlay, npay; nsfx, nsfy, nsfz], '\t')


%% plots
clf
hold on
axis vis3d
grid on
xlabel('X');
ylabel('Y');
zlabel('Z');

u = linspace(-15,15,50);
v = linspace(-15,15,50);
[u,v] = meshgrid(u, v);
[px, py, pz] = get_smpl_surf(u, v, base_surf);

u = linspace(0,2*pi,50);
v = linspace(0.01,1,50);
[u,v] = meshgrid(u,v);
[x, y, z] = get_smpl_surf(u, v, base_surf, elipse_parm);

mesh(px, py, pz, 'edgecolor', 'k')
mesh(x, y, z)
scatter3(fx, fy, fz ,... 
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','b');
scatter3(sfx, sfy, sfz ,... 
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','r'); 
scatter3(nsfx, nsfy, nsfz ,... 
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','m'); 