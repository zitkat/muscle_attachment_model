% generates elipse slapped onto cone surface

% elipse parametrization
a=1;
b=1;
parm_surf = {
    @(u, v)a*cos(u).*v + 0;
    @(u, v)b*sin(u).*v + 5;
    };

id_surf = {
    @(u, v)u;
    @(u, v)v;
    };

% cone parametrization
A = [0, 0, 0];
R = 10;
cone_surf ={ @(u, v)A(1) + R*v; %X
             @(u, v)A(2) + R*v.*cos(u); %Y
             @(u, v)A(3) + R*v.*sin(u); %Z
           };

u = linspace(0,2*pi,50);
v = linspace(0.01,1,50);
[u,v] = meshgrid(u,v);
[x, y, z] = get_smpl_surf(u, v, cone_surf, parm_surf);


u = linspace(0,2*pi,50);
v = linspace(0,10,50);
[u,v] = meshgrid(u, v);
[px, py, pz] = get_smpl_surf(u, v, cone_surf);

% elipse boundary uniform sampling with border
nbdn = 20;
nlay = 2; % number of layers
npay = 10; % number of points per layer
ru = [linspace(0,1,nbdn)'*2*pi; repmat(linspace(0,1, npay)*2*pi,1, nlay)'];
rvi = zeros(1, npay*nlay);
for i=1:nlay
    rvi((i-1)*npay+1:i*npay) = repmat(i/(nlay+1),1,npay);
end
rv = [ones([nbdn, 1]); rvi'];

% elipse boundary random sampling 
ru = [rand(20,1)*2*pi; rand(20,1)*2*pi];
rv = [ones([20, 1]); rand(20,1)];
%[ru, rv] = meshgrid(ru, rv);
[rx, ry, rz] = get_smpl_surf(ru, rv, cone_surf, parm_surf);

clf
hold on
axis vis3d
grid on
xlabel('X');
ylabel('Y');
zlabel('Z');

mesh(px, py, pz, 'edgecolor', 'k')
mesh(x, y, z)
scatter3(rx, ry, rz ,... 
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','r');
csvwrite("cone_elipse_rnd.csv", [rx, ry, rz])