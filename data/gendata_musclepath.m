clearvars

%% load surfaces parametrizations
surf_parametrizations;

%% Generate parametric sampling
start_surf = cyl_surf;
cut_surf = plane_surf;
end_surf = parab_surf;

u = linspace(0,2*pi,50);
v = linspace(0.01,1,50);
[u,v] = meshgrid(u,v);

%% Generate tranformed sampled points
nbnd = 300; % number of points on boundary
nlay = 45;  % number of layers
nplay = 45; % number of points per layer

%% START
[start_x, start_y, start_z] = get_smpl_surf(u, v, start_surf.base_surf, start_surf.elipse_parm);
[start_nsfx, start_nsfy, start_nsfz] = get_nsf_sampl(nbnd, nlay, nplay, ...
                                                        start_surf.base_surf, start_surf.elipse_parm);
o = [0 10 0];
T = [cos(pi/2) -sin(pi/2) 0;
     sin(pi/2) cos(pi/2) 0;
     0 0 1];  
T = [T;  ((eye(3) - T)*o')' + [-30 30 0]];
T = [T, [0 0 0 1]'];
Tform = affine3d(T);
[start_x, start_y, start_z] = transformPointsForward(Tform, start_x, start_y, start_z);
[start_nsfx, start_nsfy, start_nsfz] = transformPointsForward(Tform, start_nsfx, start_nsfy, start_nsfz);

%%CUT1
[cut1_x, cut1_y, cut1_z] = get_smpl_surf(u, v, cut_surf.base_surf, cut_surf.elipse_parm);
[cut1_nsfx, cut1_nsfy, cut1_nsfz] = get_nsf_sampl(nbnd, nlay, nplay, ...
                                                   cut_surf.base_surf, cut_surf.elipse_parm);
o = [0 10 0];
T = [cos(-135) -sin(-135) 0;
     sin(-135) cos(-135) 0;
     0 0 1];  
T = [T;  [0 30 0]];
T = [T, [0 0 0 1]'];
Tform = affine3d(T);
[cut1_x, cut1_y, cut1_z] = transformPointsForward(Tform, cut1_x, cut1_y, cut1_z);
[cut1_nsfx, cut1_nsfy, cut1_nsfz] = transformPointsForward(Tform, cut1_nsfx, cut1_nsfy, cut1_nsfz);

%% CUT2
[cut2_x, cut2_y, cut2_z] = get_smpl_surf(u, v, cut_surf.base_surf, cut_surf.elipse_parm);
[cut2_nsfx, cut2_nsfy, cut2_nsfz] = get_nsf_sampl(nbnd, nlay, nplay,....  
                                                        cut_surf.base_surf, cut_surf.elipse_parm);
o = [0 10 0];
T = [cos(0) -sin(0) 0;
     sin(0) cos(0) 0;
     0 0 1];  
T = [T;  ((eye(3) - T)*o')' + [10 15 0]];
T = [T, [0 0 0 1]'];
Tform = affine3d(T);
[cut2_x, cut2_y, cut2_z] = transformPointsForward(Tform, cut2_x, cut2_y, cut2_z);
[cut2_nsfx, cut2_nsfy, cut2_nsfz] = transformPointsForward(Tform, cut2_nsfx, cut2_nsfy, cut2_nsfz);

%% END
[end_x, end_y, end_z] = get_smpl_surf(u, v, end_surf.base_surf, end_surf.elipse_parm);
[end_nsfx, end_nsfy, end_nsfz] = get_nsf_sampl(nbnd, nlay, nplay, ...
                                                end_surf.base_surf, end_surf.elipse_parm);
o = [15 10 0];
T = [cos(pi/6) -sin(pi/6) 0;
     sin(pi/6) cos(pi/6) 0;
     0 0 1];  
T = [T;  ((eye(3) - T)*o')' ]; 
T = [T, [0 0 0 1]'];
Tform = affine3d(T);
[end_x, end_y, end_z] = transformPointsForward(Tform, end_x, end_y, end_z);
[end_nsfx, end_nsfy, end_nsfz] = transformPointsForward(Tform, end_nsfx, end_nsfy, end_nsfz);

%% Plots
clf
hold on
axis vis3d
grid on
xlabel('X');
ylabel('Y');
zlabel('Z');

mesh(start_x, start_y, start_z)
scatter3(start_nsfx, start_nsfy, start_nsfz ,... 
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','m'); 

mesh(cut1_x, cut1_y, cut1_z)
scatter3(cut1_nsfx, cut1_nsfy, cut1_nsfz ,... 
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','m');

mesh(cut2_x, cut2_y, cut2_z)
scatter3(cut2_nsfx, cut2_nsfy, cut2_nsfz ,... 
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','m');

mesh(end_x, end_y, end_z)
scatter3(end_nsfx, end_nsfy, end_nsfz ,... 
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','m');
% mesh(end_x, end_y, end_z, 'edgecolor', 'k')

%% Output
fold = "data\gen_data\muscle1\";
dlmwrite(strcat(fold, "start_m1_nsunf.asc"),  [nbnd, nlay, nplay; start_nsfx, start_nsfy, start_nsfz], '\t')
dlmwrite(strcat(fold, "cut1_m1_nsunf.asc") ,  [nbnd, nlay, nplay; cut1_nsfx, cut1_nsfy, cut1_nsfz], '\t')
dlmwrite(strcat(fold, "cut2_m1_nsunf.asc") ,  [nbnd, nlay, nplay; cut2_nsfx, cut2_nsfy, cut2_nsfz], '\t')
dlmwrite(strcat(fold, "end_m1_nsunf.asc")  ,  [nbnd, nlay, nplay; end_nsfx, end_nsfy, end_nsfz], '\t')
