%% saddle parametrization

saddle_surf.base_surf ={ 
             @(u, v) v; %X
             @(u, v) u; %Y
             @(u, v) u.*u/10 - v.*v/10; %Z
           }; 
saddle_surf.elipse_parm = {
    @(u, v)5*cos(u).*v;
    @(u, v)5*sin(u).*v;
    };
saddle_surf.name = 'saddle';

saddlehump_surf.base_surf ={ 
             @(u, v) v; %X
             @(u, v) u; %Y
             @(u, v) u.*u/10 - v.*v/10 + 3*exp(-(u-1).^2).*exp(-(v+.5).^2); %Z
           }; 
   
saddlehump_surf.elipse_parm = {
    @(u, v)5*cos(u).*v;
    @(u, v)5*sin(u).*v;
    };
saddlehump_surf.name = 'saddlehump';


%% paraboloid parametrization
parab_surf.base_surf ={ 
              @(u, v) + u.*u/10 + v.*v/10; %X
              @(u, v) + u; %Y
              @(u, v) + v; %Z
            };
parab_surf.elipse_parm = {
    @(u, v)2*cos(u).*v + 10;
    @(u, v)10*sin(u).*v + 2;
    };
parab_surf.name = 'parab';


%% paraboloid parametrization
parabhump_surf.base_surf ={ 
              @(u, v) + u.*u/10 + v.*v/10 - 3*exp(-(u-10).^2).*exp(-(v-2).^2); %X
              @(u, v) + u; %Y
              @(u, v) + v; %Z
            };
parabhump_surf.elipse_parm = {
    @(u, v)2*cos(u).*v + 10;
    @(u, v)10*sin(u).*v + 2;
    };
parabhump_surf.name = 'parabhump';
%% cylinder parametrization
cyl_surf.R = 10;
cyl_surf.base_surf ={ 
             @(u, v) + cyl_surf.R*v; %X
             @(u, v) + cyl_surf.R*cos(u); %Y
             @(u, v) + cyl_surf.R*sin(u); %Z
           };
cyl_surf.elipse_parm = {
    @(u, v)cos(u).*v + 0;
    @(u, v)sin(u).*v + 0;
    };
cyl_surf.name = 'cyl';


%% cone parametrization       
cone_surf.R = 10;
cone_surf.base_surf ={ 
             @(u, v) + cone_surf.R*v; %X
             @(u, v) + cone_surf.R*v.*cos(u); %Y
             @(u, v) + cone_surf.R*v.*sin(u); %Z
           };
cone_surf.elipse_parm = {
    @(u, v)cos(u).*v + 0;
    @(u, v)sin(u).*v + 5;
    };
cone_surf.name = 'cone';

%% tilted plane parametrization
plane_surf.s1 = [1, 1, 0];
plane_surf.s2 = [0, 1, 1];
plane_surf.n = cross(plane_surf.s1, plane_surf.s2)/norm(cross(plane_surf.s1, plane_surf.s2));
plane_surf.rfrm = [plane_surf.s1; plane_surf.s2; plane_surf.n]; % reference frame for plane
plane_surf.base_surf ={ 
                  @(u, v) + plane_surf.s1(1)*u + plane_surf.s2(1)*v;
                  @(u, v) + plane_surf.s1(2)*u + plane_surf.s2(2)*v;
                  @(u, v) + plane_surf.s1(3)*u + plane_surf.s2(3)*v;
           };
plane_surf.elipse_parm = {
    @(u, v)5*cos(u).*v;
    @(u, v)6*sin(u).*v;
    };
plane_surf.name = 'plane';