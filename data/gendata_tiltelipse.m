% generates elipse on titled plane 

% elipse parametrization
a=5;
b=6;
parm_surf = {
    @(u, v)a*cos(u).*v;
    @(u, v)b*sin(u).*v;
    };
% tilted plane parameters
A = [0, 0, 0];
s1 = [1, 1, 0];
s2 = [0, 1, 1];
n = cross(s1, s2)/norm(cross(s1, s2));
rfrm = [s1; s2; n]; % reference frame for plane

% tilted plane parametrization
plane_surf ={ @(u, v)A(1) + s1(1)*u + s2(1)*v;
             @(u, v)A(2) + s1(2)*u + s2(2)*v;
             @(u, v)A(3) + s1(3)*u + s2(3)*v;
           };
% tilted elipse sampling on uniform grid :)
% grid parameters
u = linspace(0,2*pi,50);
v = linspace(0.01,1,50);
[u,v] = meshgrid(u,v);

% sampling
[x, y, z] = get_smpl_surf(u, v, plane_surf, parm_surf);

% sample proj_surf
u = linspace(-15,15,50);
v = linspace(-15,15,50);
[u,v] = meshgrid(u, v);
[px, py, pz] = get_smpl_surf(u, v,plane_surf);

% elipse boundary uniform sampling with border
nbdn = 20;
nlay = 2; % number of layers
npay = 10; % number of points per layer
ru = [linspace(0,1,nbdn)'*2*pi; repmat(linspace(0,1, npay)*2*pi,1, nlay)'];
rvi = zeros(1, npay*nlay);
for i=1:nlay
    rvi((i-1)*npay+1:i*npay) = repmat(i/(nlay+1),1,npay);
end
rv = [ones([nbdn, 1]); rvi'];

% elipse boundary random sampling 
ru = [rand(20,1)*2*pi; rand(20,1)*2*pi];
rv = [ones([20, 1]); rand(20,1)];
[rx, ry, rz] = get_smpl_surf(ru, rv, plane_surf, parm_surf);

clf
hold on
axis vis3d
grid on
mesh(px, py, pz, 'edgecolor', 'k')
mesh(x, y, z)
scatter3(rx, ry, rz ,... 
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','r');
csvwrite("tilted_elipse_rnd.csv", [rx, ry, rz])