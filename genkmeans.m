function [idx, X, sumd, D, Xw] = genkmeans(C, k, varargin)
%GENKMEANS Calculates generalized k-means
%   uses specified weights, optionaly places centroids
%   at points of the input
% Options: 'weights', W - array containging weights of individual points in C
%          'inx', 'off' - to use cetroids positions computed as weighted averages of points
%                 'on'  - to move centroid to the closest point in C
%           'init', 'maxw' - initial centroids in points of C with maximal weights
%                   'rand' - randomly select centroids from C
%                   'spaced' - uniformly space centroids within C index (!)
%                   space
% Returns: idx - index of centroids for each points in C
%          X   - centroids coordinates
%          sumd - total distance of points to their centroid 
%          D - distance matrix of points to their centroids
% NOTE % Use 'init', 'maxw' for consistent results

nargs = nargin;
args = varargin;

%% Process arguments
xweights = ones(size(C, 1), 1);
inx = 0;
maxwinit = 0;
maxiter = 50;
for i=1:2:nargin - 1 - 2
    opname = args{i};
    opval = args{i+1};
    switch lower(opname)
        case 'weights'
            xweights = opval;
        case 'inx'
            if ~ischar(opval) 
               error('inx must be on|off')
            elseif ~any(strcmpi(opval, {'on', 'off'}))
                error('inx must be on|off')
            elseif strcmpi(opval, {'on'})
                inx = 1;
            end
        case 'init'
            if ~ischar(opval) 
               error('inx must be a char string')
            elseif ~any(strcmpi(opval, {'maxw', 'rand', 'spaced'}))
                error('inx must be maxw, rand, spaced')
            elseif strcmpi(opval, {'maxw'})
                maxwinit = 1;
            end
        case 'maxiter'
            if ~isnumeric(opval) 
               error('maxiter must be positive integer')
            elseif opval < 1
                error('maxiter must be at least 1')
            else
                maxiter = opval;
            end
    end
end


%% Initial centroids
n = size(C, 1);
cidx = 1:floor(n/k)-1:n; % uniformly within index space
if maxwinit
    [~, cidx] = maxk(xweights, k); % with greatest weights
end
X = C(cidx, :);
Xw = zeros(k, 1);

%% Initialize helper variables
Fkdetree = KDTreeSearcher(C);
it = 0;
D = zeros(n, k);
nochange = 0;
idx = zeros(n, 1);


% Run cycle
while (it < maxiter) && ~nochange
    it = it + 1;   
        
    % Compute distances of points to centroids
    for i=1:k
    %     for j=1:k
    %         D(j, i) = dist(X, j , cidx(i));
    %     end   
        D(:, i) = sum((C - ones([size(C, 1), 1])*X(i, :)).^2, 2); 
    end

    
    % Get closest point 
    [~, newidx] = min(D, [], 2);
    if newidx == idx
        nochange = 1;
    end
    idx = newidx;
    sumd = sum(D, 1)';

    % Compute new centroids from clusters
    for i=1:k
        Xw(i) = sum(xweights(idx==i));
        X(i, :) = sum([xweights(idx==i)/Xw(i), xweights(idx==i)/Xw(i),xweights(idx==i)/Xw(i)] .* C(idx==i, :), 1);
    end
    cidx = knnsearch(Fkdetree, X);
    X = C(cidx, :);
    

end
% [idx, X, sumd, D] = kmeans(X, k);
% if inx
%     Fkdetree = KDTreeSearcher(X);
%     X = X(knnsearch(Fkdetree, X), :);
           
end

function [y, I] = maxk(A, k)
[A, I] = sort(A);   % No UNIQUE here
y = A(end - k + 1 :end);
I = I(end - k + 1:end);
end
