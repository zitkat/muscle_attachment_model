function N = basisfun(i,u,p,U)                 
% BASISFUN  Basis function for B-Spline 
% ------------------------------------------------------------------------- 
% ADAPTATION of BASISFUN from C Routine 
% ------------------------------------------------------------------------- 
% 
% Calling Sequence: 
%  
%   N = basisfun(i,u,p,U) 
%    
%    INPUT: 
%    
%      i - knot span  ( from FindSpan() ) 
%      u - parametric point 
%      p - spline degree 
%      U - knot sequence 
%    
%    OUTPUT: 
%    
%      N - Basis functions vector[p+1] 
%    
%    Algorithm A2.2 from 'The NURBS BOOK' pg70. 



i = i + 1; 
m = length(u);
left = zeros(p+1,1);
right = zeros(p+1,1);


N(1) = 1;
for j=1:p
    left(j+1) = u - U(i+1-j); 
    right(j+1) = U(i+j) - u;
    saved = 0;
 
    for r=0:j-1
        temp = N(r+1)/(right(r+2) + left(j-r+1));
        N(r+1) = saved + right(r+2)*temp;
        saved = left(j-r+1)*temp;
    end
 
    N(j+1) = saved;
end
   




