% clearvars
B = [];
I = [];
%% Choose generated data
% surf_parametrizations;
% 
% % Choose test surface! %
% test_surf = saddlehump_surf;
% 
% % Choose test sampling! %
smpling = '3002025';
smplingtype = 'nsunf';
% 
% base_surf = test_surf.base_surf;
% elipse_parm = test_surf.elipse_parm;
% cloud_name = test_surf.name;


%% Load generated data
% C = dlmread(strcat('data\gen_data\',smpling,'\', cloud_name,'_elipse_',smplingtype,'.asc'));
% bndn = C(1,1); % first cell contains number of boundary points
% B = unique(C(2:bndn, :), 'rows'); % boundary
% I = unique(C(bndn+1:end, :), 'rows'); % inner points
% bndn = size(B, 1);
% C = C(2:end, :);
% output_name = strcat(cloud_name,smpling,smplingtype);

%% Load sample experimental data
cloud_name = 'upon_priklad';
C = dlmread(strcat('data\smpl_data\nodes_',cloud_name,'.asc'));
% C = C /100;
output_name = cloud_name;

