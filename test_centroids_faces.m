clearvars

% surf_name = 'saddlehump3002025nsunf';
[F, V] = stlread(strcat('data/smpl_data/hand/adductor_pollicis_caput_transversum.stl'));

%% compute face volumes 
Fvols = sqrt(sum((cross(V(F(:, 2), :) - V(F(:, 1), :), V(F(:, 3), :) - V(F(:, 1), :))/2).^2, 2));
Vol = sum(Fvols);
Fwgths = Fvols ;


%% compute COGs of faces
Fcog = (V(F(:, 1), :) + V(F(:, 2), :) + V(F(:, 3), :)) / 3;

nc = 5;
[idx, X, ~, ~, Xw] = genkmeans(Fcog, nc, 'weights', Fwgths, ...
    'inx', 'on', 'maxiter', 50);
% [gidx, gwX] = wkmeanssurf(V, V, F, nc);

%% Plots
figure
clf
axis vis3d
hold on
grid on
xlabel('X');
ylabel('Y');
zlabel('Z');

cmap = lines(nc);
colormap = lines(nc);
for i=1:nc
    colors(i,:) = squeeze(ind2rgb(i,cmap))';
end

C = zeros(size(F,1),1);
for i=1:nc
    scatter3(X(i, 1), X(i, 2), X(i, 3), 'r', 'filled')
%     'filled',...
%     'MarkerFaceColor', 'b', ...
%     'MarkerEdgeColor','b',...
%     'LineWidth' , 2);
    C(idx==i, 1) = i;
end

T = patch( 'Vertices', V,... 
           'Faces', F,... 
           'FaceVertexCData', C, 'FaceAlpha', .5);
shading('faceted')
% patch( 'Vertices', V,... 
%            'Faces', F,... 
%             'FaceAlpha', 0,... 
%             'EdgeColor','k', 'LineWidth',1);     
view(3)

% scatter3(Fcog(:,1), Fcog(:,2), Fcog(:,3))
