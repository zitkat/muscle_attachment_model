clearvars

oname = "1";
sname = "2";
% oX = dlmread(strcat('outputs\muscle1\',oname,'_cntrds.asc'));
oX = dlmread(strcat('data\gen_data\ptestcross',oname,'.asc'));

% sX = dlmread(strcat('outputs\muscle1\',sname,'_cntrds.asc'));
sX = dlmread(strcat('data\gen_data\ptestcross',sname,'.asc'));

scale = 3;
T = [1    0          0
     0 cos(pi/2) -sin(pi/2);
     0 sin(pi/2)  cos(pi/2);
    ];
T = [T;  [0 0 0] ]; 
T = [T, [0 0 0 1]'];
Tform = affine3d(T);
oX = transformPointsForward(Tform, oX);
nsX = pair_points(oX, sX, scale, 1);