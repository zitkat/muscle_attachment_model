clearvars
cloud_name = 'saddlehump';
load_cloud_data

surf_name = 'saddlehump3002025nsunf';
[F, V] = stlread(strcat('outputs/',surf_name,'/',surf_name, '_poisson8.stl'));


K = convhull(C(:, 1),C(:, 2),C(:, 3));
cloud_hull = C(K, :);
[clipV, clipF] = clipmesh(V, F, cloud_hull);

%% plot the clip triangulation
figure
clf
axis vis3d
hold on
grid on
xlabel('X');
ylabel('Y');
zlabel('Z');
axis equal
trisurf(clipF,clipV(:,1),clipV(:,2),clipV(:,3),...
    'facecolor','c','edgecolor','b', 'FaceAlpha', .0)
scatter3(C(:,1),C(:,2),C(:,3),20,'ko','filled')
view(3)

%% plot the points cloud
figure;
clf
axis vis3d
hold on
grid on
xlabel('X');
ylabel('Y');
zlabel('Z');
% title('Points Cloud','fontsize',14)
scatter3(C(:,1),C(:,2),C(:,3),20,'ko','filled')
trisurf(F,V(:,1),V(:,2),V(:,3),...
    'facecolor','c','edgecolor','b', 'FaceAlpha', .0)
plot3(cloud_hull(:, 1), cloud_hull(:, 2), cloud_hull(:, 3))
axis vis3d
view(3)