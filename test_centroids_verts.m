clearvars

surf_name = 'saddlehump3002025nsunf';
[F, V] = stlread(strcat('outputs/',surf_name,'/',surf_name, '_poisson15_clip.stl'));

[~, Crvs] = findPointNormals(V, 200);

nc = 10;
[~ , X] = genkmeans(V, nc, 'inx', 'on');
[idx, wX] = genkmeans(V, nc, 'weights', Crvs, 'inx', 'on', 'init', 'maxw');

%% Plots
figure
clf
axis vis3d
hold on
grid on
xlabel('X');
ylabel('Y');
zlabel('Z');

cmap = lines(nc);
colormap = lines(nc);
for i=1:nc
    colors(i,:) = squeeze(ind2rgb(i,cmap))';
end

C = zeros(size(V,1),3);
for i=1:nc
%     scatter3(X(i, 1), X(i, 2), X(i, 3), 150, 'filled',...
%     'MarkerFaceColor', cmap(i,:), ...
%     'MarkerEdgeColor','k',...
%     'LineWidth' , 2);
    scatter3(wX(i, 1), wX(i, 2), wX(i, 3), 150, 'filled',...
    'MarkerFaceColor', cmap(i,:), ...
    'MarkerEdgeColor','r',...
    'LineWidth' , 2);
%     scatter3(V(idx==i, 1), V(idx==i, 2), V(idx==i, 3), ...
%     'MarkerFaceColor', cmap(i,:), 'MarkerEdgeColor',  cmap(i,:));
%     plot3([X(i,1), wX(i, 1)], [X(i,2), wX(i, 2)], [X(i, 3), wX(i, 3)], 'b')
%     scatter3(Fcog(idx==i, 1), Fcog(idx==i, 2), Fcog(idx==i, 3), 'MarkerEdgeColor', cmap(i,:));
    C(idx==i, 1) = colors(i,1);
    C(idx==i, 2) = colors(i,2);
    C(idx==i, 3) = colors(i,3);
end
patch( 'Vertices', V, 'Faces', F, 'FaceVertexCData', C,...
       'FaceColor', 'interp', 'LineWidth',.1, 'FaceAlpha', .6);

% shading('faceted')
view(3)

%%
figure
clf
axis vis3d
hold on
grid on
xlabel('X');
ylabel('Y');
zlabel('Z');
colormap = jet;
scatter3(V(:, 1), V(:, 2), V(:, 3),10*ones(size(V,1), 1) ,abs(Crvs));
for i=1:nc
    scatter3(wX(i, 1), wX(i, 2), wX(i, 3), 150, 'filled',...
        'MarkerFaceColor', cmap(i, :), ...
        'MarkerEdgeColor','k',...
        'LineWidth' , 2);
end
trisurf(F, V(:, 1),V(:, 2), V(:, 3), Crvs,'FaceAlpha', .5 );
view(3)