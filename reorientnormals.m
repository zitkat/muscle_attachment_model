function rN = reorientnormals(C, N)
%REORIENTNORMALS starts from a point, chooses an arbitrary orientation and
%       propagates that to the closest vertices and does the same for all the vertices
%       encountered.
% C - cloud of points
% N - its normals
% Returns: rN - newly oriented normals

% init 
queue = ones(1, 1);
oriented = false(size(C,1), 1);
rN = N;
kdT = KDTreeSearcher(C);

i = 1;
while i <= size(queue, 2)
    v = C(queue(i), :); 
    i = i + 1;
    % get neighbours
    closeidx = knnsearch(kdT, v, 'k', 10);
    closeidx = closeidx(~oriented(closeidx));
    nbrhs = size(closeidx, 2);

    % orient their normals
    reoridx = sum(N(closeidx, :) .* repmat(rN(1, :), nbrhs, 1), 2) < 0;
    reoridx = closeidx(reoridx);
    rN(reoridx, :) = -rN(reoridx, :);
    oriented(closeidx) = 1;

    queue = [queue, closeidx];  
end

end

