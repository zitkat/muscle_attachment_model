function [ c ] = downsolve( L, b )
%LOWSOLVE Summary of this function goes here
%   Detailed explanation goes here

[n, n] = size(L);
c = zeros(n, 1);
c(1) = b(1)/L(1,1);
for i=2:n
    c(i) = b(i);
    for j=1:i-1
        c(i) = c(i) - L(i,j)*c(j);
    end
    c(i) = c(i)/L(i,i);
end

end

