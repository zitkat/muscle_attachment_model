function [gx, gy] = fitnonunifgrid(p)
%fitnonunifgrid creates grid in which each point has own column and row


% TODO ad option forming less dense grid
sx = sort(p(:,1));
gx = [sx(1);(sx(2:end) + sx(1:end-1))/2; sx(end)]';
sy = sort(p(:,2));
gy = [sy(1);(sy(2:end) + sy(1:end-1))/2; sy(end)]';
end

