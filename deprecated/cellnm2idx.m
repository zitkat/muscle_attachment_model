function [ci,cj] = cellnm2idx(cellNum, m, n)
%CELLNM2IDX returns i, j indicies of cell with cellNum
%   m is range of i index, n is range of j index
ci = int64(ceil(cellNum / n));
cj = cellNum - (ci-1)*n;
end

