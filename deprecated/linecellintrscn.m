function [ints] = linecellintrscn(lstart, lend, xg, yg, i, j)
%LINECELLINTRSCN Summary of this function goes here
%   Detailed explanation goes here

if nargin==4
    cellNum=i;
    smplx = length(gx) - 1;
    smply = length(gy) - 1;
    [i, j] = cellnm2idx(cellNum, smplx, smply);
end
lstart = lstart(1:2);
lend = lend(1:2);

cstarts = [xg(i)  , yg(j)  ;
        xg(i+1), yg(j)  ;
        xg(i+1), yg(j+1);
        xg(i)  , yg(j+1);  ];
cends = [xg(i+1), yg(j);
        xg(i+1), yg(j+1);
        xg(i)  , yg(j+1);
        xg(i)  , yg(j);  ];
cell = [cstarts, cends];
res = lineSegmentIntersect(cell, [lstart, lend]);

for i=1:4
    if res.intAdjacencyMatrix(i, 1)
        break
    end  
end
ints = [res.intMatrixX(i, 1) res.intMatrixY(i, 1)];  

end

