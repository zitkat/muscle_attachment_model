function isin = ispointincell(p, gx, gy, i,j)
%ISPOINTINCELL returns 1 if point is in cell with indicies i, j
%   or for just 4 argins i can be cellNum

if nargin==4
    cellNum=i;
    smplx = length(gx) - 1;
    smply = length(gy) - 1;
    [i, j] = cellnm2idx(cellNum, smplx, smply);
end

isin =  (gx(i) <= p(1)) && (p(1) <= gx(i+1)) && ...
        (gy(j) <= p(2)) && (p(2) <= gy(j+1));

end

