function [nbrhdnum] = getcellnbrhd(i, j, m, n, diam)
%GETCELLNBRHD returns neighbouring cells up to the "distance" diam
%    m is i index range, m is j index range
%   uses max metric
% TODO make this more efficient
nbrhdnum = zeros(1, 8*diam);
in = 1;

for dj = -diam : diam
    if (i + diam <= m) && (0 < j + dj) && (j + dj <= n)
        nbrhdnum(in) = (i+diam-1)*n+j+dj;
        in = in+1;
    end
    if (0 < i - diam) && (0 < j + dj) && (j + dj <= n)
        nbrhdnum(in) = (i-diam-1)*n+j+dj;
        in = in+1;
    end
end


for di = -diam+1 : diam-1
    if (0 < i + di) && (i + di <= m) && (j + diam <= n)
        nbrhdnum(in) = (i+di-1)*n+j+diam;
        in = in+1;
    end
    if (0 < i - di) && (i - di <= m) && (0 < j - diam)
        nbrhdnum(in) = (i+di-1)*n+j-diam;
        in = in+1;
    end
end
nbrhdnum = nbrhdnum(1:in-1);
end

