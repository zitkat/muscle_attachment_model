function [cellNum] = cellidx2num(i, j, n)
%CELLIDX2NUM returns cell number of cell with indicies i, j
%   n is j index range
cellNum = (i-1)*n + j;
end

