function [clipV, clipF] = clipmesh(V, F, clipHull, eps)
%clipmesh Clips given mesh to passed convex hull
%    So far extracts points within Hull and uses CRUST for new
%    triangulation.
%    TODO: retain original triangulation
if nargin==3
    eps = 0;
end
[A, b] = vert2lcon(clipHull);
is_inside = false(size(V,1), 1);
for i=1:size(V,1)
    is_inside(i) = all(A*V(i, :)'<=b + eps);
end

clipV = V(is_inside, :);
clipF = MyCrustOpen(clipV);


end

