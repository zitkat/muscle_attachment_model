function [ok_mask] = identify_turnarounds(varargin)
% identify_turnarounds for set of lines defined by keypoints, returns mask 

if nargin == 1
    error('provide at least two sets of points')
end

S = varargin{1};
ok_mask = ones(size(S, 1), 1);
for i=2:nargin - 1
    I = varargin{i};
    E = varargin{i + 1};
    v1 = I - S;
    v2 = E - I;
    
    v1dotv2 = sum(v1 .* v2, 2);
    
    ok_mask = ok_mask & (v1dotv2 > 0);
    
    S = I;
end

end

