function A = getdistmatrix(X, Y)
%GETDISTMATRIX Returns matrix of euclidean distances between points X and Y
A = zeros(size(X, 1), size(Y, 1));
for i = 1:size(X, 1)
    for j = 1:size(Y, 1)
        A(i, j) = norm(X(i, :) - Y(j, :));
    end
end
end

