function [F, V] = poissonsurfrec(C, N, depth)
%poissonsurfrec Kinda wrapper to call PoissonRecon.x64.exe
%   saves C and N to temporary file and then calls poisson
%   reconstruction on it saving result into tmptri.ply
% C - cloud of points
% N - normals - for success must be nicely combed
% depth - number iterations, see poisson reconstruction docs
% Returns: F - faces
%          V - new(!) veritces


dlmwrite('outputs\poisstmpCN.asc',  [C, N], '\t')
inarg = ' --in outputs\poisstmpCN.asc';
outarg = ' --out outputs\poisstmptri.ply';
dptharg = [' --depth' ' ' int2str(depth)];
spN = ['--samplesPerNode' ' ' num2str(15)];
cmmnd = ['libs\poisson-surf-rec\PoissonRecon.x64.exe', inarg, outarg, dptharg];
% PoissonRecon.x64.exe can be obtained from 
% http://www.cs.jhu.edu/~misha/Code/PoissonRecon/Version8.0/
system(cmmnd);
[V, F] = read_ply('outputs\poisstmptri.ply');

end

