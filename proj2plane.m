function [proj2D, proj3D] = proj2plane(n, d, points, o, v1, v2)
%PROJ2PLANE Projects points onto plane given by normal n and constant d
% returns projections of the points in provided refence frame,
% (alternatively calculates it) and in original 3D reference frame

[l, ~]= size(points);

n = n/norm(n); % to ensure n is unit vector
dists = (points*n - d);
proj3D = points - dists*n';

% create 2D basis on the plane if not provided
if  nargin==4 
    v1 = getorth3dvec(n);
    v2 = cross(n, v1);
elseif nargin==3 % calculate origin if not provided
    v1 = getorth3dvec(n);
    v2 = cross(n, v1);
    origin = mean(proj3D, 1);
    o = origin - (origin*n - d)*n';  
end

proj2D = zeros([l, 3]);
for i=1:l
    proj2D(i, 1) = v1' * (proj3D(i, :) - o)';
    proj2D(i, 2) = v2' * (proj3D(i, :) - o)';
    proj2D(i, 3) = dists(i);
end
end

