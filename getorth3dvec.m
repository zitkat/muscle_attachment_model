function [v] = getorth3dvec(n)
%GETORTH3DVEC returns 3D unit vector orthogonal to the one 
% provided, is actually quite robust, use this to get 3D reper
% from one vector withou any other restrictions

% returns canonical basis vector for 
% n = c*e_i
if n(1) == 0 && n(2)==0
     v = [1; 0; 0];
     return
end
if n(3) == 0 && n(2)==0
     v = [0; 1; 0];
     return    
end
if n(3) == 0 && n(1)==0
     v = [0; 0; 1];
     return    
end

% we now know at least two of the 
% components of n are nonzero
if n(3)==0
    v = [-n(2); n(1); 0];
elseif n(2)==0
     v = [-n(3); 0; n(1)];
else
     v = [0; -n(3); n(2)];    

%normalize as promised
v = v/norm(v);
end

