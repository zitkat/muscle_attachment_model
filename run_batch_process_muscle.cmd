@echo off
set do_plot=0
set do_exit=exit;


IF "%~1"=="-h" GOTO help

set in_folder_path=%~1
IF NOT "%~2"=="" set out_folder_path=%~2


FOR %%i IN (%in_folder_path%\*.yaml) DO run_process_muscle %%i %out_folder_path%\

GOTO end

:help
echo "    This is conveniecne script for processing muscle data in batch:               "
echo "                                                                                  "
echo "      run_batch_process_muscle [-h] <specification folder path> [outputfolder path]     "
echo "                                                                                  "
echo "      -h  this help                                                               "
echo "      specification folder path   folder containing .yaml files                     "

:end