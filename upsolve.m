function [ c ] = upsolve( U, b )
%UPSOLVE Backward solve for upper triangle matrix U vektor b
%
[n,n] = size(U);
c = zeros(n, 1);
c(n) = b(n)/U(n,n);
for i=n-1:-1:1
    c(i) = b(i);
    for j=i+1:n
        c(i) = c(i) - U(i,j)*c(j);
    end
    c(i) = c(i)/U(i,i);
end
end

