function [idx, C, sumd, D] = wkmeansgeod(X, V, F, k, varargin)
%WKMEANSGEOD computes kmeans for coloud of points X on surface given by f
%and V, uses geodesic distance and optional weights
% DOES NOT WORK!

nargs = nargin;
args = varargin;
%% Process arguments
xweights = ones(size(X, 1), 1);
maxwinit = 0;
for i=1:2:nargs - 1 - 4
    opname = args{i};
    opval = args{i+1};
    switch lower(opname)
        case 'weights'
            xweights = opval;
        case 'init'
            if ~ischar(opval) 
               error('inx must be char string')
            elseif ~any(strcmpi(opval, {'maxw', 'rand', 'spaced'}))
                error('inx must be maxw, rand, spaced')
            elseif strcmpi(opval, {'maxw'})
                maxwinit = 1;
            end
    end
end

%% Initial centroids
n = size(X, 1);
cidx = 1:floor(n/k)-1:n; % uniformly within index space
if maxwinit
    [~, cidx] = maxk(xweights, k); % with greatest weights
end
C = X(cidx, :);

%% Initialize helper variables
global geodesic_library;                
geodesic_library = 'geodesic_release';      %"release" is faster and "debug" does additional checks

Fkdetree = KDTreeSearcher(X);
maxiter = 50;
it = 0;
D = zeros(n, k);
nochange = 0;
idx = zeros(n, 1);

%% Initialization for geodesic distance
% TODO geodesic library does not seem 
%  to work on meshes that do not represent closed surface, what now?


mesh = geodesic_new_mesh(V, F);         %initilize new mesh
algorithm = geodesic_new_algorithm(mesh, 'exact');      %initialize new geodesic algorithm

%% Optimization cycle
while (it < maxiter) && ~nochange
    it = it + 1;   
        
    %% Compute distances of points to centroids
    source_points = {};
    for i=1:k
        source_points{i} = geodesic_create_surface_point('vertex', cidx(i), X);
    end
    
    %% Get closest points 
    geodesic_propagate(algorithm, source_points);
    [newidx, D] = geodesic_distance_and_source(algorithm);
    if newidx == idx
        nochange = 1;
    end
    idx = newidx;
    sumd = sum(D, 1)';

    %% Compute new centroids from clusters
    for i=1:k
        totw = sum(xweights(idx==i));
        C(i, :) = sum(xweights(idx==i)/totw .* X(idx==i, :), 1);
    end
    cidx = knnsearch(Fkdetree, C);
    C = X(cidx, :);

    
end
geodesic_delete;
end