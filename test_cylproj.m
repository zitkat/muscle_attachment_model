clearvars
B = [];
I = [];
cloud_name = 'saddlehump';
load_cloud_data


%% Coordinates transformation
trep.n = fitnormal(C);
trep.v1 = getorth3dvec(trep.n);
trep.v2 = cross(trep.n, trep.v1);
trep.o = mean(C,1);

regplane = @(s, t) cat(3, trep.o(1) + s .* trep.v1(1) + t .* trep.v2(1),...
                          trep.o(2) + s .* trep.v1(2) + t .* trep.v2(2),...
                          trep.o(3) + s .* trep.v1(3) + t .* trep.v2(3));

% TODO it's all about placing the cylinder right, so how do we do it?
T = [trep.v2, trep.v1, trep.n];  
T = [T;  (-inv(T)*trep.o')' + [0, 0, 5]];
T = [T, [0 0 0 1]'];
Tform = affine3d(T);


%% Projection on cylinder/torus
% TODO detect ocluded surface
tC = transformPointsForward(Tform, C);
ctC = proj2cyl(tC);
% pctC = projcylback([ctC(:,1), ctC(:,2), 2*ctC(:,3)]);
pctC = projcylback([ctC(:,1), ctC(:,2), ones(size(ctC(:,3))) * 2*max(ctC(:,3))]);


%% Create grid - planeproj
[pgrid, ppin, ppout, ppor, ppbnd] = creategrid(tC, [],...
    'gridstrat', 'regular', ...
    'smplx', 30,...
    'smply', 30,...
    'plot', 'off');
[psmplx, psmply, ~] = size(pgrid);


%% Create grid - cylproj
[tcgrid, cpin, cpout, cpor, cpbnd] = creategrid(ctC, [],...
    'gridstrat', 'regular', ...
    'smplx', 30,...
    'smply', 30,...
    'plot', 'on');
[csmplx, csmply, ~] = size(tcgrid);

[tcgridcut, cutouttris] = cutoutgrid(tcgrid, [cpin; cpor]);

tgrid = zeros(csmplx, csmply, 3);
for i=1:csmply
    tgrid(:, i, :) = projcylback( squeeze((tcgrid(:, i, :))));
end

sgrid = zeros(csmplx, csmply, 3);
for i=1:csmply
   sgrid(:, i, :) = transformPointsInverse(Tform, squeeze((tgrid(:, i, :))));
end
tgridcut = projcylback(tcgridcut);
gridcut = transformPointsInverse(Tform, tgridcut);


%% Grid Plots
figure
clf
axis vis3d
hold on
grid on
xlabel('X');
ylabel('Y');
zlabel('Z');
% zlim([-10,10])
%grid plots
% plotgrid(pgrid, ppin, [], ppor, ppbnd);
scatter3(C(:,1), C(:,2), C(:,3), 'k')
plotgrid(sgrid, cpin, [], cpor, cpbnd);
% mesh(squeeze(cgrid(:,:, 1)), ...
%      squeeze(cgrid(:,:, 2)), ...
%      squeeze(cgrid(:, :,3)),  'edgecolor', 'k')
 trimesh(cutouttris, gridcut(:, 1), gridcut(:, 2), gridcut(:, 3), 'FaceAlpha', .5);


%% Projection plots
figure
clf
axis vis3d
hold on
grid on
xlabel('X');
ylabel('Y');
zlabel('Z');
scatter3(pctC(:,1), pctC(:,2), pctC(:,3), 'r')
for i=1:size(tC, 1)
    plot3([tC(i,1), pctC(i, 1)], [tC(i,2), pctC(i, 2)], [tC(i,3), pctC(i, 3)], 'b')
end
scatter3(tC(:,1), tC(:,2), tC(:,3), 'k')
quiver3(0,0,0, 0,0,.1, 'LineWidth',2)
quiver3(0,0,0, 0,.1,0, 'LineWidth',2)
quiver3(0,0,0, .1,0,0, 'LineWidth',2)

% show cylinder for clarity
A = [0, 0, 0];
R = 2*max(ctC(:,3));
cylinder_surf ={ @(u, v)A(1) + R*v; %X
                 @(u, v)A(2) + R*cos(u); %Y
                 @(u, v)A(3) + R*sin(u); %Z
           };
       
u = linspace(0,2*pi,50);
v = linspace(1.1*min(ctC(:,1)),1.1*max(ctC(:,1)),50);
[u,v] = meshgrid(u, v);
[px, py, pz] = get_smpl_surf(u, v, cylinder_surf);
mesh(px, py, pz, 'edgecolor', 'b', 'FaceAlpha', 0)
quiver3(0,0,0, 0,0,R, 'LineWidth',2)
quiver3(0,0,0, 0,R,0, 'LineWidth',2)
quiver3(0,0,0, R,0,0, 'LineWidth',2)

%% Output
stlwrite(strcat('outputs/', cloud_name, '/', output_name, '_surfgrid_cyl.stl'),...
            cutouttris, gridcut)



